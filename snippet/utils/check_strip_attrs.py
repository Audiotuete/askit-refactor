from django.db import models

def check_strip_attrs(self):
  for field in self._meta.fields:
      if isinstance(field, (models.CharField, models.TextField)):
        value = getattr(self, field.name)
        if value and hasattr(value, 'strip'):
          setattr(self, field.name, value.strip())
from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from snippet.users.forms import UserChangeForm, UserCreationForm

User = get_user_model()

CUSTOM_USER_FIELDS = (
    (None, {'fields': ('current_poll', 'unique_identifier', 'pending_multipolls')}),
)


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
  # list_filter = auth_admin.UserAdmin.list_filter + ('groups__name',)
  form = UserChangeForm
  add_form = UserCreationForm
  add_fieldsets = (
      (None, {
          'classes': ('wide',),
          'fields': ('username', 'current_poll', 'password1', 'password2', ),
      }),
  )
  fieldsets = (('User', {'fields': ('name',)}),) + CUSTOM_USER_FIELDS + auth_admin.UserAdmin.fieldsets
  list_display = ['username', 'is_superuser', 'custom_group', 'email', 'date_joined', 'current_poll',
                  'unique_identifier',  'activation_key']
  search_fields = ['username', 'unique_identifier', 'email', 'current_poll__poll_name']

  def custom_group(self, obj):
    """
    get group, separate by comma, and display empty string if user has no group
    """
    return ','.join([g.name for g in obj.groups.all()]) if obj.groups.count() else ''

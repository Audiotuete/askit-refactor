import graphene
from graphql_jwt.decorators import superuser_required

# Types
from .__types import UserType

# Models
from ..models import User

class UpdateAllUsersMutation(graphene.Mutation):
  success = graphene.Boolean()

  @superuser_required
  def mutate(self, info, **kwargs):

    all_users = User.objects.filter(current_poll_id__in=[21,24,25,22,26,23,27,28,29,30,31], pending_multipolls=[])
    print(all_users)
    
    for user in all_users:
      print(user.username)
      user.pending_multipolls = [[24,0],[25,0],[22,0],[26,0],[23,0],[27,0],[28,0],[29,0],[30,0],[31,0]]
      user.save()

    success = True

    return UpdateAllUsersMutation(success=success)


class UpdateAllUsers(graphene.ObjectType):
  update_all_users = UpdateAllUsersMutation.Field()

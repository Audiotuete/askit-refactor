import graphene
from django.apps import apps as django_apps
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.crypto import get_random_string
from django.conf import settings
from django.db import transaction
from django.contrib.sites.models import Site
import math

# Types
from .__types import UserType

# Models
from ..models import User
from snippet_polls.models import Poll
from snippet_accounts.models import Account
from snippet_notifications.models import NotificationEmail


class CreateUserMutation(graphene.Mutation):
  user = graphene.Field(UserType)

  class Arguments:
    # username = graphene.String(required=True)
    # email = graphene.String(required=True)
    # password = graphene.String(required=True)
    pollCode = graphene.String(required=True)
    browserInfo = graphene.String(required=True)
    osInfo = graphene.String(required=True)
    uniqueIdentifier = graphene.String()

  def mutate(self, info, pollCode, browserInfo, osInfo, **kwargs):

    try:
      with transaction.atomic():
        current_poll = get_poll_by_code(pollCode)

        user = User(
            username=generate_random_username(),
            # email = email,
            current_poll=current_poll,
            browserInfo=browserInfo,
            osInfo=osInfo,
            unique_identifier=kwargs.get('uniqueIdentifier', None)
        )

        user.set_password(settings.USER_PASSWORD)
        user.save()

        current_poll.poll_participants_counter += 1

        account = Account.objects.select_related("tier").get(user_id=current_poll.poll_owner_id)
        current_participants_num = current_poll.poll_participants_counter
        max_participants_num = account.tier.participants_amount

        if current_participants_num == math.ceil(max_participants_num*0.8):
          send_participant_limit_mail(info, current_poll, account,  current_participants_num, max_participants_num, limit_reached=False)
        elif current_participants_num == max_participants_num:
          send_participant_limit_mail(info, current_poll, account, current_participants_num, max_participants_num, limit_reached=True)

          current_poll.poll_status = Poll.Status["INACTIVE"]

        current_poll.save()


    # If user with uniqueIdentifier already exists get this user
    except:
      user = User.objects.filter(unique_identifier=kwargs.get('uniqueIdentifier', None)).first()

    return CreateUserMutation(user=user)


class CreateUser(graphene.ObjectType):
  create_user = CreateUserMutation.Field()

# Create user helper functions


def generate_random_username():
  generated_username = "pa-" + get_random_string(length=16).lower()

  # If generated_username already exists (highly unlikely) generate new one
  while User.objects.filter(username=generated_username):
    generated_username = get_random_string(length=16).lower()

  return generated_username


def get_poll_by_code(pollCode):
  Poll = django_apps.get_model('snippet_polls', 'Poll')
  return Poll.objects.get(poll_code=pollCode)

def send_participant_limit_mail(info, current_poll, account, current_particpant_num, max_participant_num, limit_reached=False):

  if ("Schul" in account.tier.name) and not limit_reached:
    email_notification = NotificationEmail.objects.get(slug="participant-limit-close-no-updgrade")
  elif ("Schul" in account.tier.name) and limit_reached:
    email_notification = NotificationEmail.objects.get(slug="participant-limit-reached-no-updgrade")
  elif not limit_reached:
    email_notification = NotificationEmail.objects.get(slug="participant-limit-close")
  else:
    email_notification = NotificationEmail.objects.get(slug="participant-limit-reached")


  subject = email_notification.subject
  html_message = render_to_string('emails/email_main.html', {
    'domain': Site.objects.get_current().domain,
    'poll_name': current_poll.poll_name,
    'woo_subscription_id': account.woo_subscription_id,
    'current_particpant_num': current_particpant_num,
    'max_particpant_num': max_participant_num,
    'subject': email_notification.subject,
    'content': email_notification.content,
  })

  plain_message = strip_tags(html_message)
  from_email = 'info@askit-app.de'
  to = account.email
  print(to)
  send_mail(subject, plain_message, from_email, [to], html_message=html_message)


# if len(username) < 3:
#   raise Exception('Username must have at least 3 characters!')
# if len(password) < 8:
#   raise Exception('The password must be at least 8 characters long!')
# if User.objects.filter(username = username_lowercase):
#   raise Exception('Username already exists!')

from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

import re
import graphene

# Types
# Models
from ..models import User
from snippet_notifications.models import NotificationEmail



class SendConfirmationMailMutation(graphene.Mutation):
  success = graphene.Boolean()

  class Arguments:
    username = graphene.String(required=True)
    email = graphene.String(required=True)

  def mutate(self, info, username, email):

    success = valitdate_email_get_user(username, email)
    return SendConfirmationMailMutation(success=success)


class SendConfirmationMail(graphene.ObjectType):
  send_confirmation_mail = SendConfirmationMailMutation.Field()


def valitdate_email_get_user(info, username, email):
  # Validate and save email
  if not re.match(r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', email):
    return False
    # raise Exception('Email must be a vaild E-Mail-Adress!')
  else:
    user = User.objects.get(username=username)
    user.email = email
    user.save()

    poll = user.current_poll


    email_notification = NotificationEmail.objects.get(slug="email-confirmation")

    subject = email_notification.subject
    html_message = render_to_string('emails/email_main.html', {
        'domain': info.context.META.get("HTTP_ORIGIN"),
        'poll_email_header': poll.poll_email_header,
        'poll_impress': poll.poll_impress,
        'poll_participation_terms': poll.poll_participation_terms,
        'poll_privacy': poll.poll_privacy,
        'activation_key': user.activation_key,
        'subject': email_notification.subject,
        'content': email_notification.content,
    })
  # ------------------------------------------

  plain_message = strip_tags(html_message)
  from_email = 'info@askit-app.de'
  to = email
  send_mail(subject, plain_message, from_email, [to], html_message=html_message)
  return True

from django.apps import apps as django_apps
from django.contrib.auth import get_user_model

import graphene

from .__types import UserType


class VerifyUserEmailMutation(graphene.Mutation):
  user = graphene.Field(UserType)

  class Arguments:
    activation_key = graphene.String(required=True)
    poll_code = graphene.String(required=True)

  def mutate(self, info, activation_key, poll_code):

    user = validate_activation_key_then_save_email_in_UserPoll(activation_key, poll_code)

    return VerifyUserEmailMutation(user=user)


class VerifyUserEmail(graphene.ObjectType):
  verify_user_email = VerifyUserEmailMutation.Field()


def validate_activation_key_then_save_email_in_UserPoll(key, poll_code):
  User = get_user_model()
  try:
    pending_user = User.objects.get(activation_key=key)
  except:
    return None
  if pending_user.email:
    Poll = django_apps.get_model('snippet_polls', 'Poll')
    poll_id = Poll.objects.get(poll_code=poll_code)

    UserPoll = django_apps.get_model('snippet_user_polls', 'UserPoll')

    match_user_poll = UserPoll.objects.get(user=pending_user, poll_id=poll_id)
    match_user_poll.success_email = pending_user.email
    match_user_poll.save()
    return pending_user

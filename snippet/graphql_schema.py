import graphene
import graphql_jwt

from snippet_questions.graphql.__types import QuestionOpenType, QuestionYesOrNoType, QuestionMultipleType, QuestionSingleType, QuestionRangeType, QuestionExplainerType
from snippet_user_answers.graphql.__types import UserAnswerOpenType, UserAnswerYesOrNoType, UserAnswerMultipleType, UserAnswerSingleType, UserAnswerRangeType, UserAnswerExplainerType
from snippet_tiers.graphql.__types import TierType

from snippet_tans.graphql.checkTan import CheckTan

from snippet.users.graphql.createUser import CreateUser
from snippet.users.graphql.currentUser import CurrentUser
from snippet.users.graphql.sendConfirmationMail import SendConfirmationMail
from snippet.users.graphql.updateUserEmail import UpdateUserEmail
from snippet.users.graphql.updateUserPendingMultipolls import UpdateUserPendingMultipolls

from snippet.users.graphql.verifyUserEmail import VerifyUserEmail

from snippet_accounts.graphql.createAccount import CreateAccount
from snippet_accounts.graphql.currentAccount import CurrentAccount
from snippet_accounts.graphql.updateAccount import UpdateAccount

from snippet_questions.graphql.questionsByPoll import QuestionsByPoll
from snippet_questions.graphql.updateNgramsQuestionOpen import UpdateNgramsQuestionOpen

from snippet_questions.graphql.createQuestionMultiple import CreateQuestionMultiple
from snippet_questions.graphql.createQuestionSingle import CreateQuestionSingle
from snippet_questions.graphql.createQuestionRange import CreateQuestionRange
from snippet_questions.graphql.createQuestionYesOrNo import CreateQuestionYesOrNo
from snippet_questions.graphql.createQuestionOpen import CreateQuestionOpen

from snippet_questions.graphql.updateQuestionMultiple import UpdateQuestionMultiple
from snippet_questions.graphql.updateQuestionSingle import UpdateQuestionSingle
from snippet_questions.graphql.updateQuestionRange import UpdateQuestionRange
from snippet_questions.graphql.updateQuestionYesOrNo import UpdateQuestionYesOrNo
from snippet_questions.graphql.updateQuestionOpen import UpdateQuestionOpen
from snippet_questions.graphql.deleteQuestion import DeleteQuestion

from snippet_images.graphql.imagesByUser import ImagesByUser

from snippet_images.graphql.uploadImage import UploadImage
from snippet_images.graphql.deleteImage import DeleteImage
from snippet_images.graphql.purgeImageCache import PurgeImageCache

from snippet_polls.graphql.aPoll import APoll
from snippet_polls.graphql.pollsByUser import PollsByUser

from snippet_polls.graphql.createPoll import CreatePoll
from snippet_polls.graphql.duplicatePoll import DuplicatePoll
from snippet_polls.graphql.joinPoll import JoinPoll
from snippet_polls.graphql.updatePollSettings import UpdatePollSettings
from snippet_polls.graphql.updatePollQuestionOrder import UpdatePollQuestionOrder
from snippet_polls.graphql.updatePollFilterQuestion import UpdatePollFilterQuestion
from snippet_polls.graphql.changePollStatus import ChangePollStatus
from snippet_polls.graphql.resetPoll import ResetPoll
from snippet_polls.graphql.deletePoll import DeletePoll

from snippet_user_polls.graphql.userPolls import UserPolls

from snippet_user_answers.graphql.userAnswersByUser import UserAnswersByUser
from snippet_user_answers.graphql.userAnswersByQuestion import UserAnswersByQuestion
from snippet_user_answers.graphql.userAnswersByQuestionCounted import UserAnswersByQuestionCounted
from snippet_user_answers.graphql.ngramsUserAnswersOpen import NgramsUserAnswersOpen

from snippet_user_answers.graphql.updateUserAnswerOpen import UpdateUserAnswerOpen
from snippet_user_answers.graphql.updateUserAnswerYesOrNo import UpdateUserAnswerYesOrNo
from snippet_user_answers.graphql.updateUserAnswerMultiple import UpdateUserAnswerMultiple
from snippet_user_answers.graphql.updateUserAnswerSingle import UpdateUserAnswerSingle
from snippet_user_answers.graphql.updateUserAnswerRange import UpdateUserAnswerRange
from snippet_user_answers.graphql.updateUserAnswerExplainer import UpdateUserAnswerExplainer

from snippet_impress_privacy.graphql.askitImpress import AskitImpress
from snippet_notifications.graphql.allHints import AllHints


class Query(
    CurrentUser,
    CurrentAccount,
    # AllUsers,
    APoll,
    PollsByUser,
    UserPolls,
    QuestionsByPoll,
    ImagesByUser,
    UserAnswersByUser,
    UserAnswersByQuestion,
    UserAnswersByQuestionCounted,
    NgramsUserAnswersOpen,
    AskitImpress,
    AllHints,

    # -----------------------
    graphene.ObjectType
):
  pass


class Mutation(
    CheckTan,
    CreateUser,
    SendConfirmationMail,
    UpdateUserEmail,
    UpdateUserPendingMultipolls,
    VerifyUserEmail,

    CreateAccount,
    UpdateAccount,

    CreatePoll,
    DuplicatePoll,
    JoinPoll,
    UpdatePollSettings,
    UpdatePollQuestionOrder,
    UpdatePollFilterQuestion,
    ChangePollStatus,
    ResetPoll,
    DeletePoll,

    UpdateUserAnswerOpen,
    UpdateUserAnswerYesOrNo,
    UpdateUserAnswerMultiple,
    UpdateUserAnswerSingle,
    UpdateUserAnswerRange,
    UpdateUserAnswerExplainer,

    UpdateNgramsQuestionOpen,

    CreateQuestionMultiple,
    CreateQuestionSingle,
    CreateQuestionRange,
    CreateQuestionYesOrNo,
    CreateQuestionOpen,

    UpdateQuestionMultiple,
    UpdateQuestionSingle,
    UpdateQuestionRange,
    UpdateQuestionYesOrNo,
    UpdateQuestionOpen,

    DeleteQuestion,

    UploadImage,
    DeleteImage,
    PurgeImageCache,

    # -----------------------
    graphene.ObjectType
):
  token_auth = graphql_jwt.ObtainJSONWebToken.Field()
  verify_token = graphql_jwt.Verify.Field()
  refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(
    query=Query,
    mutation=Mutation,
    types=[
        TierType,

        QuestionOpenType,
        QuestionYesOrNoType,
        QuestionMultipleType,
        QuestionSingleType,
        QuestionRangeType,
        QuestionExplainerType,

        UserAnswerOpenType,
        UserAnswerYesOrNoType,
        UserAnswerMultipleType,
        UserAnswerSingleType,
        UserAnswerRangeType,
        UserAnswerExplainerType
    ])


FROM python:3.8-slim-buster

ENV PYTHONUNBUFFERED 1


RUN apt-get update \
  # dependencies for building Python packages
  && apt-get install -y build-essential \
  # CRON
  # && apt-get install -y cron && service cron start && service cron status \ 
  # psycopg2 dependencies
  && apt-get install -y libpq-dev \
  # Translations dependencies
  && apt-get install -y gettext \
  # cleaning up unused files
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && rm -rf /var/lib/apt/lists/* 


RUN addgroup --system django \
    && adduser --system --ingroup django django

# Requirements are installed here to ensure they will be cached.
COPY ./requirements /requirements
RUN pip install --no-cache-dir -r /requirements/production.txt \
    && rm -rf /requirements

# Download NTLK Data for NLP 
RUN [ "python", "-c", "import nltk; import os; nltk.download('stopwords', download_dir='/home/django/nltk_data/'); nltk.download('punkt', download_dir='/home/django/nltk_data/')" ]

COPY ./compose/staging/django/entrypoint /entrypoint
RUN sed -i 's/\r$//g' /entrypoint
RUN chmod +x /entrypoint
RUN chown django /entrypoint

COPY ./compose/staging/django/start /start
RUN sed -i 's/\r$//g' /start
RUN chmod +x /start
RUN chown django /start

# COPY ./compose/staging/django/cron /cron
# RUN sed -i 's/\r$//g' /cron
# RUN chmod +x /cron
# RUN chown django /cron

# # Setuid to enable cron for non-root users 
# RUN chmod u+s /usr/sbin/cron

COPY --chown=django:django . /app
USER django

WORKDIR /app

ENTRYPOINT ["/entrypoint"]

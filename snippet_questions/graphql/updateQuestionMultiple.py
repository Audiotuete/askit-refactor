import graphene
from graphql_jwt.decorators import login_required

import datetime

# Models
from ..models import QuestionMultiple


class UpdateQuestionMultipleMutation(graphene.Mutation):
  question_type = graphene.String()
  question_text = graphene.String()
  question_imagelink = graphene.String()
  options = graphene.String()
  pass

  class Arguments:
    question_id = graphene.ID(required=True)
    question_text = graphene.String(required=True)
    question_imagelink = graphene.String(required=True)
    options = graphene.List(graphene.String, default_value=[])

  @login_required
  def mutate(self, info, question_id, question_text, question_imagelink, options):
    pending_question = QuestionMultiple.objects.get(id=question_id)

    pending_question.question_text = question_text
    pending_question.question_imagelink = question_imagelink

    pending_question.options = options

    pending_question.save()

    return UpdateQuestionMultipleMutation(
        question_type=pending_question.question_type,
        question_text=pending_question.question_text,
        question_imagelink=pending_question.question_imagelink,
        options=pending_question.options
    )


class UpdateQuestionMultiple(graphene.ObjectType):
  update_question_multiple = UpdateQuestionMultipleMutation.Field()

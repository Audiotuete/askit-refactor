from django.apps import apps as django_apps
import datetime

import graphene
from graphql_jwt.decorators import login_required

# Models
from ..models import QuestionOpen


class CreateQuestionOpenMutation(graphene.Mutation):
  id = graphene.Int()
  question_type = graphene.String()
  question_text = graphene.String()
  question_imagelink = graphene.String()
  pass

  class Arguments:
    poll_id = graphene.ID(required=True)
    question_text = graphene.String(required=True)
    question_imagelink = graphene.String(required=True)

  @login_required
  def mutate(self, info, poll_id, question_text, question_imagelink):

    Poll = django_apps.get_model('snippet_polls', 'Poll')

    question = QuestionOpen(
        poll=Poll.objects.get(id=poll_id),
        question_type="Open",
        question_text=question_text,
        question_imagelink=question_imagelink,
    )
    question.save()

    return CreateQuestionOpenMutation(
        id=question.id,
        question_type=question.question_type,
        question_text=question.question_text,
        question_imagelink=question.question_imagelink,
    )


class CreateQuestionOpen(graphene.ObjectType):
  create_question_open = CreateQuestionOpenMutation.Field()

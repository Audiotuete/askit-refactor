from django.apps import apps as django_apps

import graphene
from graphql_jwt.decorators import login_required

from itertools import chain

# Types
from .__types import BaseQuestionType

# Models
from ..models import QuestionOpen, QuestionYesOrNo, QuestionMultiple, QuestionSingle, QuestionRange, QuestionExplainer


class QuestionsByPoll(graphene.ObjectType):
  questions_by_poll = graphene.List(BaseQuestionType, poll_id=graphene.ID(required=True))

  @login_required
  def resolve_questions_by_poll(self, info, poll_id, **kwargs):

    PollModel = django_apps.get_model('snippet_polls', 'Poll')
    current_poll = PollModel.objects.get(id=poll_id)

    open_questions = QuestionOpen.objects.filter(poll_id=poll_id)
    yes_no_questions = QuestionYesOrNo.objects.filter(poll_id=poll_id)
    multi_questions = QuestionMultiple.objects.filter(poll_id=poll_id)
    single_questions = QuestionSingle.objects.filter(poll_id=poll_id)
    range_questions = QuestionRange.objects.filter(poll_id=poll_id)
    explainer_questions = QuestionExplainer.objects.filter(poll_id=poll_id)

    # multi_questions = QuestionMultiple.objects.all()
    # yes_no_questions = QuestionYesOrNo.objects.all()
    # open_questions = QuestionOpen.objects.all()

    # pk_list = [208, 209, 210, 211, 212]

    # for question in multi_questions:
    #   for i, option in enumerate(question.options):
    #     question.options[i] = option.replace("&#44;", ",")
    #   question.save()

    ordered_question_ids = current_poll.ordered_question_ids
    # ordered_question_ids = []

    questions_by_poll = list(chain(
        open_questions,
        yes_no_questions,
        multi_questions,
        single_questions,
        range_questions,
        explainer_questions
    ))

    if len(questions_by_poll) == len(ordered_question_ids):
      questions_by_poll = sorted(questions_by_poll, key=lambda question: ordered_question_ids.index(question.id))
    else:
      print("WARNING: ordered_question_ids does not contain all question ids in poll")

    return questions_by_poll

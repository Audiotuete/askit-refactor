from django.contrib import admin

from snippet_questions.models import Question, QuestionOpen, QuestionYesOrNo, QuestionMultiple, QuestionSingle, QuestionRange, QuestionExplainer


class QuestionAdmin(admin.ModelAdmin):
  model = Question
  list_display = ('id', 'question_type', 'question_text', 'poll')
  readonly_fields = [
      'poll',
      'question_type',
      # 'question_videolink',
  ]

  def has_add_permission(self, request):
    return False


class QuestionOpenAdmin(admin.ModelAdmin):
  list_display = ('question_text', 'poll', 'question_type')
  readonly_fields = ['question_type', ]

  def get_form(self, request, obj=None, **kwargs):
    """Override the get_form and extend the 'exclude' keyword arg"""
    if obj:
      kwargs.update({
          'exclude': getattr(kwargs, 'exclude', tuple()) + ('poll', 'question_type'),
      })
    return super(QuestionOpenAdmin, self).get_form(request, obj, **kwargs)


class QuestionYesOrNoAdmin(admin.ModelAdmin):
  list_display = ('question_text', 'poll', 'question_type')
  readonly_fields = ['question_type', ]

  def get_form(self, request, obj=None, **kwargs):
    """Override the get_form and extend the 'exclude' keyword arg"""
    if obj:
      kwargs.update({
          'exclude': getattr(kwargs, 'exclude', tuple()) + ('poll', 'question_type'),
      })
    return super(QuestionYesOrNoAdmin, self).get_form(request, obj, **kwargs)


class QuestionMultipleAdmin(admin.ModelAdmin):
  list_display = ('question_text', 'poll', 'question_type')
  readonly_fields = ['question_type', ]

  def get_form(self, request, obj=None, **kwargs):
    """Override the get_form and extend the 'exclude' keyword arg"""
    if obj:
      kwargs.update({
          'exclude': getattr(kwargs, 'exclude', tuple()) + ('poll', 'question_type'),
      })
    return super(QuestionMultipleAdmin, self).get_form(request, obj, **kwargs)

class QuestionSingleAdmin(admin.ModelAdmin):
  list_display = ('question_text', 'poll', 'question_type')
  readonly_fields = ['question_type', ]

  def get_form(self, request, obj=None, **kwargs):
    """Override the get_form and extend the 'exclude' keyword arg"""
    if obj:
      kwargs.update({
          'exclude': getattr(kwargs, 'exclude', tuple()) + ('poll', 'question_type'),
      })
    return super(QuestionSingleAdmin, self).get_form(request, obj, **kwargs)

class QuestionRangeAdmin(admin.ModelAdmin):
  list_display = ('question_text', 'poll', 'question_type')
  readonly_fields = ['question_type', ]

  def get_form(self, request, obj=None, **kwargs):
    """Override the get_form and extend the 'exclude' keyword arg"""
    if obj:
      kwargs.update({
          'exclude': getattr(kwargs, 'exclude', tuple()) + ('poll', 'question_type'),
      })
    return super(QuestionRangeAdmin, self).get_form(request, obj, **kwargs)

class QuestionExplainerAdmin(admin.ModelAdmin):
  list_display = ('question_text', 'poll', 'question_type')
  readonly_fields = ['question_type', ]

  def get_form(self, request, obj=None, **kwargs):
    """Override the get_form and extend the 'exclude' keyword arg"""
    if obj:
      kwargs.update({
          'exclude': getattr(kwargs, 'exclude', tuple()) + ('poll', 'question_type'),
      })
    return super(QuestionExplainerAdmin, self).get_form(request, obj, **kwargs)

admin.site.register(Question, QuestionAdmin)
admin.site.register(QuestionOpen, QuestionOpenAdmin)
admin.site.register(QuestionYesOrNo, QuestionYesOrNoAdmin)
admin.site.register(QuestionMultiple, QuestionMultipleAdmin)
admin.site.register(QuestionSingle, QuestionSingleAdmin)
admin.site.register(QuestionRange, QuestionRangeAdmin)
admin.site.register(QuestionExplainer, QuestionExplainerAdmin)

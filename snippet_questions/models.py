from django.apps import apps as django_apps
from django.db import models
from django.dispatch import receiver

from django.contrib.postgres.fields import ArrayField, JSONField
from ckeditor.fields import RichTextField

from snippet.utils.check_strip_attrs import check_strip_attrs


class Question(models.Model):
  poll = models.ForeignKey('snippet_polls.Poll', on_delete=models.CASCADE,
                           verbose_name="Poll (not changeable after creation)")
  question_type = models.CharField(max_length=150, default='')
  question_text = models.TextField(max_length=250)
  question_imagelink = models.CharField(max_length=250, null=True, blank=True)
  pub_date = models.DateTimeField(auto_now_add=True)

  __poll_pre_save = None

  def __str__(self):
    return self.question_text

  def save(self, *args, **kwargs):
    # When Question doesn't exist create Question, assign question type to new Question and generate UserAnswers for every question/user in the poll
    if self.pk == None:
      self.save_and_create_useranswers_for_questions(self)

      PollModel = django_apps.get_model('snippet_polls', 'Poll')

      poll = PollModel.objects.get(id=self.poll_id)
      poll.ordered_question_ids.append(self.id)
      poll.save()

    else:
      check_strip_attrs(self)
      super(Question, self).save(*args, **kwargs)

  def save_and_create_useranswers_for_questions(self, *args, **kwargs):
    question_model_name = self.__class__.__name__
    self.question_type = question_model_name[8:]
    
    check_strip_attrs(self)
    super(Question, self).save(*args, **kwargs)

    models_dict = {
        'QuestionOpen': 'UserAnswerOpen',
        'QuestionYesOrNo': 'UserAnswerYesOrNo',
        'QuestionMultiple': 'UserAnswerMultiple',
        'QuestionSingle': 'UserAnswerSingle',
        'QuestionRange': 'UserAnswerRange',
        'QuestionExplainer': 'UserAnswerExplainer',
    }

    UserAnswerModel = django_apps.get_model('snippet_user_answers', models_dict[question_model_name])
    UserPoll = django_apps.get_model('snippet_user_polls', 'UserPoll')

    all_user_polls = UserPoll.objects.filter(poll=self.poll)

    user_answer_list = []
    for user_poll in all_user_polls:
      user_answer_list.append(UserAnswerModel(user_id=user_poll.user_id, poll_id=user_poll.poll_id, question=self))

    UserAnswerModel.objects.bulk_create(user_answer_list)


@receiver(models.signals.post_delete, sender=Question)
def delete_question_from_poll(sender, instance, **kwargs):
  PollModel = django_apps.get_model('snippet_polls', 'Poll')
  poll = PollModel.objects.get(id=instance.poll_id)

  if instance.id in poll.ordered_question_ids:
    poll.ordered_question_ids.remove(instance.id)
    poll.save()


class QuestionYesOrNo(Question):
  pass


class QuestionOpen(Question):
  ngrams = JSONField(null=True, blank=True)
  filter_user_ids_length = models.IntegerField(blank=True, null=True)

class QuestionMultiple(Question):
  options = ArrayField(models.CharField(max_length=150, blank=True), default=list, null=True)


class QuestionSingle(Question):
  options = ArrayField(models.CharField(max_length=150, blank=True), default=list, null=True)


class QuestionRange(Question):
  class ScaleType(models.TextChoices):
    INTERVAL = 'interval', "Schritte"
    CONTINUOUS = 'continuous', "Fließend (1-10)" 

  scale_type = models.CharField(("Skalentyp"), max_length=10, choices=ScaleType.choices, default=ScaleType.INTERVAL)
  scale = ArrayField(models.CharField(max_length=150, blank=True), default=list, null=True)

class QuestionExplainer(Question):
  explainer_text = RichTextField(("Text"), blank=True, default="")
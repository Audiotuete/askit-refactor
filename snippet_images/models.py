from django.db import models
from django.conf import settings
import uuid
from django.dispatch import receiver


def user_directory_path(instance, _filename):
  # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
  chars = {'ö': 'oe', 'ä': 'ae', 'ü': 'ue', 'ß': 'ss'}
  filename = instance.title.lower()
  for char in chars:
    filename = filename.replace(char, chars[char])

  if instance.owner:
    return 'images/{0}/{1}'.format(instance.owner.username, filename)
  else:
    return 'images/public/{0}'.format(filename)


class Image(models.Model):

  uuid = models.UUIDField(
      primary_key=True, default=uuid.uuid4, editable=False,
  )
  owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
  title = models.CharField(("Titel"), max_length=100)
  alt_text = models.CharField(("Alt Text"), max_length=100, null=True, blank=True)
  created_at = models.DateTimeField(auto_now_add=True)
  src = models.FileField(upload_to=user_directory_path)
  width = models.IntegerField(null=True, blank=True)
  height = models.IntegerField(null=True, blank=True)


  def __str__(self):
    return self.title

  # def save(self, *args, **kwargs):
  #   super(Image, self).save(*args, **kwargs)

  # def delete(self, *args, **kwargs):
  #   super(Image, self).delete(*args, **kwargs)


@receiver(models.signals.post_delete, sender=Image)
def remove_file_from_s3(sender, instance, using, **kwargs):
  instance.src.delete(save=False)

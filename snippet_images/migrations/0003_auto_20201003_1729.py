# Generated by Django 3.0.5 on 2020-10-03 15:29

from django.db import migrations, models
import snippet_images.models


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_images', '0002_auto_20201003_1718'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='data',
            field=models.FileField(upload_to=snippet_images.models.user_directory_path),
        ),
    ]

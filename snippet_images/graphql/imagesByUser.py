import graphene
from graphql_jwt.decorators import login_required

# Types
from .__types import ImageType

# Models
from ..models import Image


class ImagesByUser(graphene.ObjectType):
  images_by_user = graphene.List(ImageType)

  @login_required
  def resolve_images_by_user(self, info, **kwargs):
    if info.context.user.is_superuser:
      return Image.objects.all().order_by('-created_at')
    else:
      return Image.objects.filter(owner=info.context.user).order_by('-created_at')

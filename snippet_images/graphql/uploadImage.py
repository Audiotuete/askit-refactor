
import graphene
from graphql_jwt.decorators import login_required
from graphene_file_upload.scalars import Upload

from ..models import Image
from .__types import ImageType

class UploadImageMutation(graphene.Mutation):
  image = graphene.Field(ImageType)
  
  class Arguments:
    title = graphene.String(required=True)
    width = graphene.Int(required=True)
    height = graphene.Int(required=True)
    src = Upload(required=True)

  @login_required
  def mutate(self, info, title, src, width, height):
    image = Image(
        owner=info.context.user,
        title=title,
        src=src,
        width=width,
        height=height
    )
    image.save()
    return UploadImageMutation(image=image)

class UploadImage(graphene.ObjectType):
  upload_image = UploadImageMutation.Field()

from django.apps import AppConfig


class SnippetImagesConfig(AppConfig):
    name = 'snippet_images'

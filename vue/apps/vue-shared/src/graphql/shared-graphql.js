// Auth
import GET_TOKEN from "./auth/getToken.gql"
import VERIFY_TOKEN from "./auth/verifyToken.gql"

// UserPolls
import USER_POLLS from "./userPolls/userPolls.gql"

// Tans
import CHECK_TAN from "./tans/checkTan.gql"

// Users
import CURRENT_USER from "./users/currentUser.gql"
import CURRENT_ACCOUNT from "./users/currentAccount.gql"
import CREATE_USER from "./users/createUser.gql"
import UPDATE_USER_EMAIL from "./users/updateUserEmail.gql"
import VERIFY_USER_EMAIL from "./users/verifyUserEmail.gql"
import SEND_CONFIRMATION_MAIL from "./users/sendConfirmationMail.gql"
import UPDATE_USER_PENDING_MULTIPOLLS from "./users/updateUserPendingMultipolls.gql"

// Polls
import A_POLL from "./polls/aPoll.gql"
import POLLS_BY_USER from "./polls/pollsByUser.gql"

import CREATE_POLL from "./polls/createPoll.gql"
import DUPLICATE_POLL from "./polls/duplicatePoll.gql"
import JOIN_POLL from "./polls/joinPoll.gql"
import UPDATE_POLL_FILTER_QUESTION from "./polls/udpatePollFilterQuestion.gql"
import UPDATE_POLL_QUESTION_ORDER from "./polls/updatePollQuestionOrder.gql"
import UPDATE_POLL_SETTINGS from "./polls/updatePollSettings.gql"
import CHANGE_POLL_STATUS from "./polls/changePollStatus.gql"
import RESET_POLL from "./polls/resetPoll.gql"
import DELETE_POLL from "./polls/deletePoll.gql"

// Questions
import QUESTIONS_BY_POLL from "./questions/questionsByPoll.gql"

import CREATE_QUESTION_SINGLE from "./questions/createQuestionSingle.gql"
import CREATE_QUESTION_MULTIPLE from "./questions/createQuestionMultiple.gql"
import CREATE_QUESTION_RANGE from "./questions/createQuestionRange.gql"
import CREATE_QUESTION_YES_OR_NO from "./questions/createQuestionYesOrNo.gql"
import CREATE_QUESTION_OPEN from "./questions/createQuestionOpen.gql"

import UPDATE_QUESTION_SINGLE from "./questions/updateQuestionSingle.gql"
import UPDATE_QUESTION_MULTIPLE from "./questions/updateQuestionMultiple.gql"
import UPDATE_QUESTION_RANGE from "./questions/updateQuestionRange.gql"
import UPDATE_QUESTION_YES_OR_NO from "./questions/updateQuestionYesOrNo.gql"
import UPDATE_QUESTION_OPEN from "./questions/updateQuestionOpen.gql"
import UPDATE_NGRAMS_QUESTION_OPEN from "./questions/updateNgramsQuestionOpen.gql"

import DELETE_QUESTION from "./questions/deleteQuestion.gql"

// Images
import IMAGES_BY_USER from "./images/imagesByUser.gql"

import UPLOAD_IMAGE from "./images/uploadImage.gql"
import DELETE_IMAGE from "./images/deleteImage.gql"
import PURGE_IMAGE_CACHE from "./images/purgeImageCache.gql"

// UserAnswers
import ANALYTICS_USER_ANSWERS_BY_QUESTION from "./userAnswers/analyticsUserAnswersByQuestion.gql"

import USER_ANSWERS_BY_QUESTION from "./userAnswers/userAnswersByQuestion.gql"
import USER_ANSWERS_BY_QUESTION_COUNTED from "./userAnswers/userAnswersByQuestionCounted.gql"
import USER_ANSWERS_BY_USER from "./userAnswers/userAnswersByUser.gql"

import UPDATE_USER_ANSWER_EXPLAINER from "./userAnswers/updateUserAnswerExplainer.gql"
import UPDATE_USER_ANSWER_MULTIPLE from "./userAnswers/updateUserAnswerMultiple.gql"
import UPDATE_USER_ANSWER_OPEN from "./userAnswers/updateUserAnswerOpen.gql"
import UPDATE_USER_ANSWER_RANGE from "./userAnswers/updateUserAnswerRange.gql"
import UPDATE_USER_ANSWER_SINGLE from "./userAnswers/updateUserAnswerSingle.gql"
import UPDATE_USER_ANSWER_YES_OR_NO from "./userAnswers/updateUserAnswerYesOrNo.gql"

import NGRAMS_USER_ANSWERS_OPEN from "./userAnswers/ngramsUserAnswersOpen.gql"

import ASKIT_IMPRESS_PRIVACY from "./impressPrivacy/askitImpressPrivacy.gql"
import ALL_HINTS from "./notifications/allHints.gql"

export {
  GET_TOKEN,
  VERIFY_TOKEN,
  USER_POLLS,
  CHECK_TAN,
  CURRENT_USER,
  CURRENT_ACCOUNT,
  CREATE_USER,
  UPDATE_USER_EMAIL,
  VERIFY_USER_EMAIL,
  SEND_CONFIRMATION_MAIL,
  UPDATE_USER_PENDING_MULTIPOLLS,
  UPDATE_POLL_FILTER_QUESTION,
  A_POLL,
  POLLS_BY_USER,
  CREATE_POLL,
  DUPLICATE_POLL,
  JOIN_POLL,
  UPDATE_POLL_QUESTION_ORDER,
  UPDATE_POLL_SETTINGS,
  CHANGE_POLL_STATUS,
  RESET_POLL,
  DELETE_POLL,
  QUESTIONS_BY_POLL,
  CREATE_QUESTION_SINGLE,
  CREATE_QUESTION_MULTIPLE,
  CREATE_QUESTION_RANGE,
  CREATE_QUESTION_YES_OR_NO,
  CREATE_QUESTION_OPEN,
  UPDATE_QUESTION_SINGLE,
  UPDATE_QUESTION_MULTIPLE,
  UPDATE_QUESTION_RANGE,
  UPDATE_QUESTION_YES_OR_NO,
  UPDATE_QUESTION_OPEN,
  DELETE_QUESTION,
  IMAGES_BY_USER,
  UPLOAD_IMAGE,
  DELETE_IMAGE,
  PURGE_IMAGE_CACHE,
  ANALYTICS_USER_ANSWERS_BY_QUESTION,
  USER_ANSWERS_BY_QUESTION,
  USER_ANSWERS_BY_QUESTION_COUNTED,
  USER_ANSWERS_BY_USER,
  UPDATE_USER_ANSWER_EXPLAINER,
  UPDATE_USER_ANSWER_MULTIPLE,
  UPDATE_USER_ANSWER_OPEN,
  UPDATE_USER_ANSWER_RANGE,
  UPDATE_USER_ANSWER_SINGLE,
  UPDATE_USER_ANSWER_YES_OR_NO,
  NGRAMS_USER_ANSWERS_OPEN,
  UPDATE_NGRAMS_QUESTION_OPEN,
  ASKIT_IMPRESS_PRIVACY,
  ALL_HINTS,
}

module.exports = {
  chainWebpack: (config) => {
    // Fix Safari Hot-Reload bug
    if (process.env.NODE_ENV === "development") {
      config.output.filename("[name].[hash].js").end()
    }
    // GraphQL Loader
    config.module
      .rule("graphql")
      .test(/\.(gql|graphql)$/)
      .use("graphql-tag/loader")
      .loader("graphql-tag/loader")
      .end()
    // Fonts loader
    config.module
      .rule("fonts")
      .test(/\.(woff|woff2|eot|ttf|otf)$/)
      .use("file-loader")
      .loader("file-loader")
      .end()
  },
}

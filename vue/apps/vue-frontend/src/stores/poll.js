import { defineStore } from "pinia"

import apolloProvider from "@/apollo.config"

import { A_POLL } from "vue-shared"
import { CHECK_TAN } from "vue-shared"
import { CREATE_USER } from "vue-shared"
import { GET_TOKEN } from "vue-shared"

export const usePollAppStore = defineStore({
  id: "pollApp",
  state: () => ({
    activePoll: {},
    code: {
      minCodeLength: 5,
      pendingPollCode: "",
      showPollCodeInvalidMessage: false,
    },
    tan: {
      showTanForm: false,
      tanInput: "",
      showTanUsedMessage: "",
      tanVerified: false,
    },
  }),
  getters: {},
  actions: {
    async setPoll(urlParam) {
      try {
        if (urlParam.match(/^([a-z0-9_-]){5}$/)) {
          await apolloProvider.defaultClient
            .query({
              query: A_POLL,
              variables: {
                pollCode: urlParam,
              },
            })
            .then((poll) => {
              this.$patch({ activePoll: poll.data.aPoll })
              return poll.data.aPoll
            })
            .catch((error) => {
              console.error(error)
            })
        } else {
          throw "Invalid pollCode"
        }
      } catch {
        throw "Invalid pollCode"
      }
    },
    startPoll() {
      if (this.activePoll.pollRequiresTan && !this.tan.tanVerified) {
        this.tan.showTanForm = true
      } else {
        let mutationParams = {
          mutation: CREATE_USER,
          variables: {
            pollCode: this.activePoll.pollCode,
            browserInfo: "this.getBrowserInfo()",
            osInfo: "navigator.platform",
          },
        }

        apolloProvider.defaultClient
          .mutate(mutationParams)
          .then((data) => {
            this.login(data.data.createUser.user.username)
          })
          .catch((error) => {
            console.error(error)
          })
      }
    },
    login(username) {

      apolloProvider.defaultClient
        .mutate({
          mutation: GET_TOKEN,
          variables: {
            username: username,
            password: process.env.VUE_APP_USER_PASSWORD,
          },
        })
        .then((data) => {
          let pollCodeParam = window.location.pathname.split("/")[1]
          const token = data.data.tokenAuth.token
          localStorage.setItem(process.env.VUE_APP_TOKEN_PREFIX + pollCodeParam, token)
          if (localStorage.getItem(process.env.VUE_APP_TOKEN_PREFIX + pollCodeParam)) {
            this.router.push("/umfrage")
          }
        })
        .catch((error) => {
          console.error(error)
          // We restore the initial user input
        })
    },
    checkTan() {
      apolloProvider.defaultClient
        .mutate({
          mutation: CHECK_TAN,
          variables: {
            pollId: this.activePoll.id,
            tan: this.tan.tanInput,
          },
        })
        .then((data) => {
          this.tan.tanVerified = data.data.checkTan.success
          localStorage.setItem("snippet-tan", this.tan.tanInput)

          if (!this.tan.tanVerified) {
            this.tan.showTanUsedMessage = "USED"
          } else {
            setTimeout(() => {
              this.startPoll()
            }, 1000)
          }
        })
        .catch(() => {
          this.tan.showTanUsedMessage = "INVALID"
        })
    },
  },
})

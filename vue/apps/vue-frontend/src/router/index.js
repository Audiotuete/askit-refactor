import Vue from "vue"
import VueRouter from "vue-router"

import Welcome from "../screens/welcome/Welcome"
// import TutorialScreen from "../screens/TutorialScreen"
import SwiperScreen from "../screens/SwiperScreen"
import EndScreen from "../screens/EndScreen"
import VerificationScreen from "../screens/VerificationScreen"

import apolloProvider from "../apollo.config"
import { VERIFY_TOKEN } from "vue-shared"

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      name: "Welcome",
      path: "/",
      components: { default: Welcome },
      meta: { visitNotAuthGuard: true, visitPollClosedGuard: true },
    },
    // {
    //   name: "TutorialScreen",
    //   path: "/tutorial",
    //   components: { default: TutorialScreen },
    //   meta: { visitAuthGuard: true, visitPollClosedGuard: true },
    // },
    {
      name: "SwiperScreen",
      path: "/umfrage",
      component: SwiperScreen,
      meta: { visitAuthGuard: true, visitPollClosedGuard: true },
    },
    {
      name: "VerificationScreen",
      path: "/verify/:activation_key/:poll_code",
      components: { default: VerificationScreen },
    },
    {
      name: "EndScreen",
      path: "/ende",
      components: { default: EndScreen },
    },
  ],
})

router.beforeEach(async (to, from, next) => {
  // Stop routing and redirect to EndScreen if poll is closed.
  // Checked when routing to WelcomeScreen, TutorialScreen, SwiperScreen
  if (to.matched.some((record) => record.meta.visitPollClosedGuard) && pollIsClosed()) {
    next("/ende") // EndScreen

    // Stop routing and redirect to SwiperScreen if user is authenticated.
    // Checked when routing to WelcomeScreen
  } else if (
    to.matched.some((record) => record.meta.visitNotAuthGuard) &&
    (await isAuthenticated())
  ) {
    next("/umfrage") // SwiperScreen

    // Stop routing and redirect WelcomeScreen if user is NOT authenticated.
    // Checked when routing to TutorialScreen, SwiperScreen
  } else if (
    to.matched.some((record) => record.meta.visitAuthGuard) &&
    !(await isAuthenticated())
  ) {
    next("/") // WelcomeScreen

    // You made it without beeing redirected. Congratulations!
  } else {
    next()
  }
})

const pollCode = window.location.pathname.split("/")[1]

function pollIsClosed() {
  return localStorage.getItem("closed-" + pollCode)
}

async function isAuthenticated() {
  const token = localStorage.getItem(process.env.VUE_APP_TOKEN_PREFIX + pollCode)

  if (!token) return false

  try {
    // Deconstructing nested Object => { data: { verifyToken } } https://medium.com/@pyrolistical/destructuring-nested-objects-9dabdd01a3b8
    const {
      data: { verifyToken },
    } = await apolloProvider.defaultClient.mutate({
      mutation: VERIFY_TOKEN,
      variables: {
        token: token,
      },
    })
    return verifyToken.payload ? true : false
  } catch {
    return false
  }
}

export default router

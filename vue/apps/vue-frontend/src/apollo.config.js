// import fetch from 'unfetch'

import Vue from "vue"
import VueApollo from "vue-apollo"

// import store from './store'
import introspectionQueryResultData from "../../vue-shared/src/graphql/fragmentTypes.json"

import { ApolloClient } from "apollo-client"
import { createHttpLink } from "apollo-link-http"
import { ApolloLink } from "apollo-link"
import { setContext } from "apollo-link-context"
import { InMemoryCache, IntrospectionFragmentMatcher } from "apollo-cache-inmemory"

import { disableFragmentWarnings } from "graphql-tag"
disableFragmentWarnings()

// import { onError } from "apollo-link-error";

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData,
})
const cache = new InMemoryCache({ fragmentMatcher })

let urlParam = window.location.pathname.split("/")[1]

// If pollCode is not after VUE_APP_URL, take the last string in URL
if (!urlParam) {
  urlParam = window.location.href.split("/").pop()
}

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem(process.env.VUE_APP_TOKEN_PREFIX + urlParam)
  return {
    headers: {
      ...headers,
      authorization: token ? `JWT ${token}` : "",
    },
  }
})

const httpLink = createHttpLink({ uri: process.env.VUE_APP_GRAPHQL_ENDPOINT })

const link = ApolloLink.from([
  authLink,
  httpLink,
  // errorLink,
])

// Create the apollo client
const apolloClient = new ApolloClient({
  link,
  cache,

  connectToDevTools: true,
})

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

// Install the vue plugin
Vue.use(VueApollo)

// const errorLink = onError(({ graphQLErrors, networkError }) => {
//   if (graphQLErrors)
//     graphQLErrors.forEach(({ message, locations, path }) =>
//       console.log(
//         `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
//       )
//     );
//   if (networkError) console.log(`[Network error]: ${networkError}`);
// });

export default apolloProvider

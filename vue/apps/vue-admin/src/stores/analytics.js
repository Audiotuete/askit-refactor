import { defineStore } from "pinia"
import { usePollStore } from "@/stores/polls"

import apolloProvider from "@/apollo.config"

import { ANALYTICS_USER_ANSWERS_BY_QUESTION } from "vue-shared"
import { USER_ANSWERS_BY_QUESTION_COUNTED } from "vue-shared"

export const useAnalyticsStore = defineStore({
  id: "analytics",
  state: () => ({
    showChartDialog: false,
    isLoadingChart: false,

    userAnswerCollection: {
      question: {
        id: 0,
        questionType: "",
        questionPosition: "",
        questionText: "",
      },
      formatedOptions: [],
      userAnswersCounted: [],
      participantsCount: 0,
    },

    showSummaryDialog: false,
    isGeneratingSummary: false,
    summaryPDF: null,
    summaryPDFChartIndex: 0,
  }),
  getters: {},
  actions: {
    getUserAnswersByQuestion(pos, question) {
      return new Promise((resolve, reject) => {
        this.showChartDialog = true
        this.isLoadingChart = true

        // Reset this.rawUserAnwsers
        this.$patch({
          userAnswerCollection: {
            question: {
              id: 0,
              questionText: "",
              questionType: "",
            },
          },
        })

        let queryVariables = {
          questionId: question.id,
          questionType: question.questionType,
        }

        // Check if conditionals are set
        const pendingPoll = usePollStore().activePoll

        if (pendingPoll.pollFilterQuestionUserIds && pendingPoll.pollFilterQuestionUserIds.length) {
          queryVariables.userIds = pendingPoll.pollFilterQuestionUserIds
        }

        if (question.questionType !== "Open") {
          apolloProvider.defaultClient
            .query({
              query: USER_ANSWERS_BY_QUESTION_COUNTED,
              fetchPolicy: "network-only",
              variables: queryVariables,
            })
            .then((data) => {
              // extract negative num of participants from data and set it absolute (always last element and negative)
              this.userAnswerCollection.participantsCount = Math.abs(
                data.data.userAnswersByQuestionCounted.slice(-1)
              )

              // extract the counted userAnswers from data
              this.userAnswerCollection.userAnswersCounted =
                data.data.userAnswersByQuestionCounted.slice(0, -1)

              this.userAnswerCollection.question.id = question.id
              this.userAnswerCollection.question.questionText = question.questionText
              this.userAnswerCollection.question.questionType = question.questionType
              if (queryVariables.userIds) {
                this.userAnswerCollection.filterQuestionUserIds = queryVariables.userIds
              }

              let position = (pos + 1).toString()
              let formatedPosition = position < 10 ? "0" + position : position
              this.userAnswerCollection.question.questionPosition = formatedPosition

              // Count answering participants

              // Format options and scale (replace "&#44" with "," and ' with " ) + parse JSON
              switch (question.questionType) {
                case "Single":
                case "Multiple":
                  this.formatQuestionOptions(question, "options")
                  break
                case "Range":
                  this.formatQuestionOptions(question, "scale")
                  break
              }
              this.isLoadingChart = false
              resolve()
            })
            .catch((error) => {
              console.error(error)
              reject()
            })
        } else {
          apolloProvider.defaultClient
            .query({
              query: ANALYTICS_USER_ANSWERS_BY_QUESTION,
              fetchPolicy: "network-only",
              variables: queryVariables,
            })
            .then((data) => {
              this.userAnswerCollection.answers = data.data.userAnswersByQuestion

              this.userAnswerCollection.question.id = question.id
              this.userAnswerCollection.question.questionText = question.questionText
              this.userAnswerCollection.question.questionType = question.questionType
              if (queryVariables.userIds) {
                this.userAnswerCollection.filterQuestionUserIds = queryVariables.userIds
              }

              let position = (pos + 1).toString()
              let formatedPosition = position < 10 ? "0" + position : position
              this.userAnswerCollection.question.questionPosition = formatedPosition

              // Count answering participants
              let counter = 0
              this.userAnswerCollection.answers.forEach((userAnswer) => {
                if (userAnswer.status) {
                  counter += 1
                }
              })
              this.userAnswerCollection.participantsCount = counter
              this.isLoadingChart = false

              // Wait 2 seconds so that canvasToPDF is only called when the hidden wordcloud animation is finished.
              setTimeout(() => {
                resolve()
              }, 2000)
            })
            .catch((error) => {
              console.error(error)
              reject()
            })
        }
      })
    },
    formatQuestionOptions(data, arrayName) {
      let formatedOptions = JSON.parse(data[arrayName].replace(/&#44;/g, ",").replace(/'/g, '"'))
      this.userAnswerCollection.formatedOptions = formatedOptions
    },
  },
})

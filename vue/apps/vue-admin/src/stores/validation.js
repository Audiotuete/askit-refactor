import { defineStore } from "pinia"

import { usePollStore } from "@/stores/polls"
import { useQuestionStore } from "@/stores/questions"

export const useValidationStore = defineStore({
  id: "validation",
  state: () => ({
    pollFields: {
      missingQuestions: false,
      missingImpress: false,
      missingPrivacy: false,
      missingEmailUsage: false,
    },
  }),
  getters: {},
  actions: {
    hasPollQuestions() {
      if (useQuestionStore().questions.length == 0) {
        this.pollFields.missingQuestions = true
        return false
      } else {
        this.pollFields.missingQuestions = false
        return true
      }
    },
    hasValidPollSettings() {
      let flag = true
      const pendingPoll = usePollStore().activePoll

      if (!pendingPoll.pollImpress) {
        this.pollFields.missingImpress = true
        flag = false
      }
      if (
        pendingPoll.pollImpress.includes("Name:", "Anschrift:", "E-Mail:", "Sonstiges:") &&
        pendingPoll.pollImpress.length == 128
      ) {
        this.pollFields.missingImpress = true
        flag = false
      }
      if (pendingPoll.pollSuccessButton == "SEND_MAIL") {
        if (!pendingPoll.pollPrivacy) {
          this.pollFields.missingPrivacy = true
          flag = false
        }
        if (!pendingPoll.pollEmailUsage) {
          this.pollFields.missingEmailUsage = true
          flag = false
        }
      }
      return flag
    },
  },
})

import { defineStore } from "pinia"
import { useTierStore } from "@/stores/tier"
import { useAppStore } from "@/stores/app"
import { usePollStore } from "@/stores/polls"
import { useQuestionStore } from "@/stores/questions"
import { useAnalyticsStore } from "@/stores/analytics"

import apolloProvider from "@/apollo.config"

import { UPDATE_POLL_FILTER_QUESTION } from "vue-shared"

export const useFilterStore = defineStore({
  id: "filter",
  state: () => ({
    showFilterDialog: false,
    showFilterSettingsMissing: false,

    filterQuestionGroup: {
      YesOrNo: { label: "Ja/Nein", questions: [] },
      Single: { label: "Einfachauswahl", questions: [] },
      Multiple: { label: "Mehrfachauswahl", questions: [] },
    },
    filterRulesQuestion: {},
    filterRulesSelectedOptions: [],
    filterResetBackup: { question: {}, selectedValues: [] },
  }),
  getters: {},
  actions: {
    openFilterSettings() {
      if (!useTierStore().tier.conditionalAnalysis) {
        useAppStore().toggleLimitDialog({ limitType: "analysis" })
      } else {
        this.$patch({
          filterResetBackup: {
            question: this.filterRulesQuestion,
            selectedValues: this.filterRulesSelectedOptions,
          },
          showFilterDialog: true,
        })
      }
    },
    closeFilterSettings() {
      this.$patch({
        filterRulesQuestion: this.filterResetBackup.question,
        filterRulesSelectedOptions: this.filterResetBackup.selectedValues,
        showFilterDialog: false,
        showFilterSettingsMissing: false,
      })
    },
    populateFilterOptions() {
      useQuestionStore().questions.forEach((question) => {
        let choiceValues = this.convertQuestionOptionsToObjects(question)
        this.addFilterOptionByType(question, choiceValues)
        if (question.id == usePollStore().activePoll.pollFilterQuestionId) {
          this.applySavedFilter(question, choiceValues)
        }
      })
    },
    convertFilterOptionsToStrings() {
      // Converts the filter options which are used as an array [{value: Number, label: String}] into an array of strings.
      // This is done because the DB only accepts string-arrays not object-arrays. Opposite is done in function below.
      return this.filterRulesSelectedOptions.map((object) => object.value)
    },
    convertQuestionOptionsToObjects(question) {
      // Converts the question options which in the DB are saved as an array of strings into an array of [{value: Number, label: String}].
      // This is done because the el-select component from element UI only works with objects of type {value: Number, label: String}.
      // Opposite is done in function above.
      let choiceValues = []
      switch (question.questionType) {
        case "YesOrNo":
          // Predefined choiceValues for YesOrNo Type
          choiceValues = [
            { value: 0, label: "Nein" },
            { value: 1, label: "Ja" },
          ]
          break
        case "Single":
        case "Multiple":
          let parsedOptions = JSON.parse(question.options.replace(/&#44;/g, ",").replace(/'/g, '"'))
          // Convert integer-array (quesiton.options) into object-array for element-ui el-select.
          // This is reversed in applyFilter() -> UPDATE_POLL_FILTER_QUESTION mutation
          parsedOptions.forEach((item, index) => {
            choiceValues.push({ value: index, label: item })
          })
          break
      }
      return choiceValues
    },
    addFilterOptionByType(question, choiceValues) {
      if (["YesOrNo", "Single", "Multiple"].includes(question.questionType)) {
        this.filterQuestionGroup[question.questionType].questions.push({
          value: question.id,
          label: question.questionText,
          type: question.questionType,
          choiceValues: choiceValues,
        })
      }
    },
    applySavedFilter(question, choiceValues) {
      this.filterRulesQuestion = {
        value: question.id,
        label: question.questionText,
        type: question.questionType,
        choiceValues: choiceValues,
      }

      let selectedValues = choiceValues.filter((item) => {
        if (usePollStore().activePoll.pollFilterQuestionSelectedValues.includes(item.value)) {
          return item
        }
      })
      this.filterRulesSelectedOptions = selectedValues
    },
    applyFilter(reset = false) {
      // Always delete generated summary
      useAnalyticsStore().$patch({
        summaryPDF: null,
        summaryPDFChartIndex: 0,
      })

      if (this.filterRulesSelectedOptions.length == 0) {
        return (this.showFilterSettingsMissing = true)
      }

      let selectedValues = this.convertFilterOptionsToStrings()

      apolloProvider.defaultClient
        .mutate({
          mutation: UPDATE_POLL_FILTER_QUESTION,
          variables: {
            pollId: usePollStore().activePoll.id,
            questionId: this.filterRulesQuestion.value,
            questionType: this.filterRulesQuestion.type,
            selectedValues: selectedValues,
            reset: reset,
          },
        })
        .then((data) => {
          usePollStore().updatePollFilterQuestionFields({
            id: reset ? null : this.filterRulesQuestion.value,
            selectedValues: reset ? [] : selectedValues,
            type: reset ? null : this.filterRulesQuestion.type,
            userIds: reset ? [] : data.data.updatePollFilterQuestion.userIds,
          })

          if (reset == true) {
            this.filterRulesQuestion = {}
            this.filterRulesSelectedOptions = []
          }
          this.showFilterDialog = false
        })
        .catch((error) => {
          console.error(error)
        })
    },
  },
})

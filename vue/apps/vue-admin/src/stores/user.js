import { defineStore } from "pinia"

import { getToken, setToken, removeToken } from "@/utils/auth"
import { resetRouter } from "@/router"

import apolloProvider from "@/apollo.config"

import { GET_TOKEN } from "vue-shared"

export const useUserStore = defineStore({
  id: "user",
  state: () => ({
    token: getToken(),
    username: "",
  }),
  getters: {},
  actions: {
    async login(userInfo) {
      const { username, password } = userInfo
      return apolloProvider.defaultClient
        .mutate({
          mutation: GET_TOKEN,
          variables: {
            username: username,
            password: password,
          },
        })
        .then((data) => {
          const token = data.data.tokenAuth.token
          setToken(token)
          this.token = token
        })
        .catch((error) => {
          console.log(error)
          throw error
          // We restore the initial user input
        })
    },

    logout() {
      removeToken() // must remove  token  first
      resetRouter()
      localStorage.removeItem("snippet-poll-id")
      // commit('RESET_STATE')
      // Reload the entire app to clean the tenacious apollo and vuex state
      location.reload()
    },

    resetToken() {
      removeToken() // must remove  token  first
      this.$reset()
      resolve()
    },
  },
})

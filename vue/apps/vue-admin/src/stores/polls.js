import { defineStore } from "pinia"
import { useTierStore } from "@/stores/tier"

import apolloProvider from "@/apollo.config"
import { CHANGE_POLL_STATUS } from "vue-shared"

export const usePollStore = defineStore({
  id: "poll",
  state: () => ({
    polls: [],
    activePoll: {
      id: 0,
      pollStatus: "",
      pollCode: "",
      orderedQuestionIds: [],
      pollName: "",
      pollDescription: "",
      pollLanguage: "",
      pollColorPrimary: "",
      pollColorSecundary: "",
      pollImpress: "",
      pollSupport: "",
      pollPrivacy: "",
      pollStartscreen: "",
      pollMissingModal: "",
      pollSuccessModal: "",
      pollSuccessButton: "",
      pollRedirectUrl: "",
      pollRedirectBtnText: "",
      pollEmailUsage: "",
      pollEmailHeader: "",
      pollParticipationTerms: "",
      pollParticipantsCounter: 0,
      pollSuccessEmailsCounter: 0,
      pollSuccessMails: [],
      pollFilterQuestionId: null,
      pollFilterQuestionType: null,
      pollFilterQuestionSelectedValues: null,
      pollFilterQuestionUserIds: null,
      __typename: "",
    },
    pendingPollStatus: "",
    showStatusDialog: false,
    showStatusChangedDialog: false,
  }),
  getters: {},
  actions: {
    changePollStatus() {
      apolloProvider.defaultClient
        .mutate({
          mutation: CHANGE_POLL_STATUS,
          variables: {
            pollId: this.activePoll.id,
            pollStatus: this.pendingPollStatus,
            resetPollFlag: false,
          },
        })
        .then(() => {
          this.activePoll.pollStatus = this.pendingPollStatus
          this.showStatusDialog = false
          this.showStatusChangedDialog = true
        })
    },
    participantLimitAt(percentage) {
      return (
        this.activePoll.pollParticipantsCounter >=
        useTierStore().tier.participantsAmount * (percentage * 0.01)
      )
    },
    setActivePoll(id) {
      localStorage.setItem("snippet-poll-id", id)
      this.activePoll = this.polls.find((x) => x.id == id)
      this.pendingPollStatus = this.activePoll.pollStatus
    },
    updatePollFilterQuestionFields(data) {
      this.activePoll.pollFilterQuestionId = data.id
      this.activePoll.pollFilterQuestionSelectedValues = data.selectedValues
      this.activePoll.pollFilterQuestionType = data.type
      this.activePoll.pollFilterQuestionUserIds = data.userIds
    },
  },
})

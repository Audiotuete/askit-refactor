// const rows = [
//   ["name1", "city1", "some other info"],
//   ["name2", "city2", "more info"]
// ];

// converts array into semicolon separated .txt or .csv file
export function userAnswersToText(format, userAnswers, fileName) {
  let cleared_newlines = userAnswers.map((answer) => [answer.map((e) => e.replace(/\n/g, " "))])
  let semicolon_seperator = cleared_newlines.map((e) => e.map((e) => e.join(";")))
  let csvContent =
    `data:text/${format};charset=utf-8,` + semicolon_seperator.map((e) => e.join()).join("\n")
  downloadFile(csvContent, fileName)
}

export function successMailsToCSV(successMails, fileName) {
  let csvContent =
    "data:text/csv;charset=utf-8," + successMails.map((address) => address.email).join("\n")
  downloadFile(csvContent, fileName)
}

function downloadFile(csvContent, fileName) {
  var encodedUri = encodeURI(csvContent)
  var link = document.createElement("a")
  link.setAttribute("href", encodedUri)
  link.setAttribute("download", fileName)
  document.body.appendChild(link) // Required for FF

  link.click() // This will download the data file named "[fileName].csv".}
  document.body.removeChild(link)
}

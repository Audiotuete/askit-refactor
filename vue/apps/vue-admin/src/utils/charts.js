import { jsPDF } from "jspdf"
import Canvg from "canvg"

export function chartToPDF(questionType, xPos, yPos, questionText, summaryPDF = null) {
  let queryString = ""
  switch (questionType) {
    case "YesOrNo":
      //queryString = "doughnut-chart"
      queryString = "pie-chart"
      break
    case "Single":
      queryString = "horizontalbar-chart"
      break
    case "Multiple":
      queryString = "horizontalbar-chart"
      break
    case "Range":
      queryString = "bar-chart"
      break
  }
  let canvas = document.querySelector(`.${queryString}-container #${queryString}`)

  canvasToPDF(canvas, xPos, yPos, 0.35, questionText, summaryPDF)
}

export function wordcloudToPDF(queryString, xPos, yPos, questionText, summaryPDF = null) {
  const canvas = document.createElement("canvas")
  canvas.width = 3508
  canvas.height = 2480

  const ctx = canvas.getContext("2d")
  ctx.setTransform(3, 0, 0, 3, 0, 0)

  let svg = document.querySelector(`.${queryString} g g`)
  let s = new XMLSerializer()
  let SVGstring = s.serializeToString(svg)

  let v = Canvg.fromString(ctx, `<svg xmlns="http://www.w3.org/2000/svg">${SVGstring}</svg>`, {
    ignoreMouse: true,
    ignoreDimensions: true,
    ignoreClear: true,
    ignoreAnimation: true,
  })
  v.start()

  canvasToPDF(canvas, xPos, yPos, 0.27, questionText, summaryPDF)
}

export function chartToPNG(questionType, questionText) {
  let queryString = ""
  switch (questionType) {
    case "YesOrNo":
      queryString = "pie-chart"
      break
    case "Single":
      queryString = "horizontalbar-chart"
      break
    case "Multiple":
      queryString = "horizontalbar-chart"
      break
    case "Range":
      queryString = "bar-chart"
      break
  }

  let canvas = document.querySelector(`.${queryString}-container #${queryString}`)

  canvas.toBlob(function (blob) {
    downloadBlob(blob, questionText)
  })
}

export function wordcloudToSVG(queryString, questionText) {
  let svg = document.querySelector(`.${queryString} svg`)
  let s = new XMLSerializer()
  let SVGstring = s.serializeToString(svg)
  let blob = new Blob([SVGstring], { type: "image/svg+xml;charset=utf-8" })

  downloadBlob(blob, questionText)
}

// Helper functions
// ------------------------------------------
function canvasToPDF(canvas, xPos, yPos, scaleFactor, questionText, summaryPDF) {
  let canvasImg = canvas.toDataURL("image/png", 1.0)
  //creates PDF from img
  let doc = null

  if (summaryPDF) {
    doc = summaryPDF
  } else {
    doc = new jsPDF({
      orientation: "l",
      unit: "px",
      format: "a4",
      putOnlyUsedFonts: true,
      floatPrecision: 16, // or "smart", default is 16
    })
  }

  doc.addImage(
    canvasImg,
    "PNG",
    xPos,
    yPos,
    canvas.width * scaleFactor,
    canvas.height * scaleFactor,
    "",
    "SLOW"
  )
  doc.setFontSize(20)
  doc.text(30, 60, questionText, { maxWidth: 600 })

  if (!summaryPDF) {
    doc.save(questionText.substring(0, questionText.length - 1))
  }
}

function downloadBlob(blob, questionText) {
  let url = window.URL.createObjectURL(blob)
  let a = document.createElement("a")
  a.href = url
  a.download = questionText.substring(0, questionText.length - 1)
  document.body.appendChild(a) // Required for FF
  a.click()
  a.remove()
  window.URL.revokeObjectURL(url)
}

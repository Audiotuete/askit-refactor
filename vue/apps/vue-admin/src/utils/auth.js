// import Cookies from 'js-cookie'

const TokenKey = process.env.VUE_APP_TOKEN_KEY

export function getToken() {
  return localStorage.getItem(TokenKey)
}

export function setToken(token) {
  return localStorage.setItem(TokenKey, token)
}

export function removeToken() {
  return localStorage.removeItem(TokenKey)
}

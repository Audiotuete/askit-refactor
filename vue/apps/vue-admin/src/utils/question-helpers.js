// Parse JSON String and convert primitives in array into objects for vue reactivity
// Vue doesn't support reactivity for primitive array elements [1,2,3] only for objects [{value: 1}, {value: 2}, {value: 3}]
export function convertJsonToObjectArray(value) {
  // Replace ASCII comma and single quotes with double
  return JSON.parse(value.replace(/&#44;/g, ",").replace(/'/g, '"')).map((step) => ({
    value: step,
  }))
}
// Convert objects in array into primitives for saving to the database
export function convertObjectArrayToNormalArray(array) {
  let sanitizedArray = array.map((element, index) => {
    if (element.value) {
      return element.value.toString().replace(/,/g, "&#44;").replace(/"/g, "'")
    } else {
      return `#${index + 1}`
    }
  })
  return sanitizedArray
}

export function generateDefaultContinuousScale() {
  let defaultContinuousScale = []
  for (var i = 1; i <= 10; i++) {
    defaultContinuousScale.push({ value: `${i}` })
  }
  return defaultContinuousScale
}

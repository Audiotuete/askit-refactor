import Layout from "@/layout"
import apolloProvider from "@/apollo.config"
import router from "@/router"

import { usePollStore } from "@/stores/polls"
import { useTierStore } from "@/stores/tier"
import { useHintStore } from "@/stores/hints"

import { POLLS_BY_USER } from "vue-shared"

export function loadPolls() {
  const asyncRoutes = {
    path: "/umfrage",
    name: "umfrage",
    component: Layout,
    redirect: "/umfrage",
    meta: { title: "Umfragen", icon: "table" },
    children: [],
  }

  apolloProvider.defaultClient
    .query({
      query: POLLS_BY_USER,
      fetchPolicy: "network-only",
    })
    .then((data) => {
      data.data.pollsByUser.forEach((poll) => {
        asyncRoutes.children.push({
          path: poll.id,
          name: "Umfrage_" + poll.id,
          props: { poll: poll },
          component: () => import("@/views/poll/Poll"),
          meta: { title: poll.pollName, icon: "" },
        })
      })
      router.addRoutes([asyncRoutes])

      let vm = router.app
      let firstPollId = data.data.pollsByUser.length > 0 ? data.data.pollsByUser[0].id : null
      let currentPollId = localStorage.getItem("snippet-poll-id")
        ? localStorage.getItem("snippet-poll-id")
        : firstPollId

      useHintStore().getHints()
      useTierStore().getTier()
      usePollStore().$patch({ polls: data.data.pollsByUser })

      if (localStorage.getItem("snippet-poll-id")) {
        populatePoll(vm, currentPollId)
      } else if (currentPollId) {
        populatePoll(vm, currentPollId)
      } else {
        setTimeout(() => router.push("/"), 200)
      }
    })
    .catch((error) => {
      console.error(error)
    })
}

function populatePoll(vm, id) {
  usePollStore().setActivePoll(id)

  setTimeout(() => router.push("/umfrage/" + id), 200)
}

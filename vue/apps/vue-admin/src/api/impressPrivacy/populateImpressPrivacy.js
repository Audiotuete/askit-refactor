import { ASKIT_IMPRESS_PRIVACY } from "vue-shared"

export async function populateImpressPrivacy(context) {
  let response = await context.$apollo.query({
    query: ASKIT_IMPRESS_PRIVACY,
    fetchPolicy: "network-only",
  })

  context.askitImpress = response.data.askitImpressPrivacy.askitImpress.content
  context.askitPrivacyPolicy = response.data.askitImpressPrivacy.askitPrivacyPolicy.intro
  context.askitPrivacyTopics = response.data.askitImpressPrivacy.askitPrivacyPolicy.privacytopicSet
}

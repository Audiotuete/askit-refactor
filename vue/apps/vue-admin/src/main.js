import Vue from "vue"
import VeeValidate from "vee-validate"
import VueI18n from "vue-i18n"

import "normalize.css/normalize.css" // A modern alternative to CSS resets

import ElementUI from "element-ui"
import "./theme/index.css"
import locale from "element-ui/lib/locale/lang/de" // lang i18n

import ChartJsPluginDataLabels from "chartjs-plugin-datalabels"

import "@/styles/index.scss" // global css

import App from "./App"

import { createPinia, PiniaVuePlugin } from "pinia"

import router from "./router"
import apolloProvider from "./apollo.config"

import "@/icons" // icon
import "@/permission" // permission control
import "@/components/global"

const Quill = require("quill")

const icons = Quill.import("ui/icons")

for (let i = 1; i <= 6; i += 1) {
  icons.header[i] = `H${i}`
}

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
Vue.use(VueI18n)
Vue.use(VeeValidate, { fieldsBagName: "veeFields" })

Vue.use(PiniaVuePlugin)
const pinia = createPinia()

import { messages } from "vue-shared"

const i18n = new VueI18n({
  locale: "de",
  fallbackLocale: "de",
  messages,
})

Vue.config.productionTip = false

new Vue({
  el: "#app",
  router,
  i18n,
  pinia,
  apolloProvider,
  components: {
    ChartJsPluginDataLabels,
  },
  render: (h) => h(App),
})

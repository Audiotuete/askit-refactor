from django.apps import AppConfig

class SnippetUserAnswersConfig(AppConfig):
    name = 'snippet_user_answers'

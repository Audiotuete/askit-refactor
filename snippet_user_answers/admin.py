from django.contrib import admin

from .models import UserAnswerOpen, UserAnswerYesOrNo, UserAnswerMultiple, UserAnswerSingle, UserAnswerRange, UserAnswerExplainer

class UserAnswerOpenAdmin(admin.ModelAdmin):
  model = UserAnswerOpen
  list_display = ['user', 'poll', 'question', 'answer_text', 'status', 'count_touched']
  actions = None

  def has_add_permission(self, request):
    return False

  def has_change_permission(self, request, obj=None):
    return False


class UserAnswerYesOrNoAdmin(admin.ModelAdmin):
  model = UserAnswerYesOrNo
  list_display = ['user', 'poll', 'question', 'answer_value', 'answer_note', 'status', 'count_touched']
  actions = None

  def has_add_permission(self, request):
    return False

  def has_change_permission(self, request, obj=None):
    return False


class UserAnswerMultipleAdmin(admin.ModelAdmin):
  model = UserAnswerMultiple
  list_display = ['user', 'poll', 'question', 'answer_choice_keys', 'status', 'count_touched']
  actions = None

  def has_add_permission(self, request):
    return False

  def has_change_permission(self, request, obj=None):
    return False


class UserAnswerSingleAdmin(admin.ModelAdmin):
  model = UserAnswerSingle
  list_display = ['user', 'poll', 'question', 'answer_choice_key', 'status', 'count_touched']
  actions = None

  def has_add_permission(self, request):
    return False

  def has_change_permission(self, request, obj=None):
    return False

class UserAnswerRangeAdmin(admin.ModelAdmin):
  model = UserAnswerRange
  list_display = ['user', 'poll', 'question', 'answer_scale_key', 'status', 'count_touched']
  actions = None

  def has_add_permission(self, request):
    return False

  def has_change_permission(self, request, obj=None):
    return False

class UserAnswerExplainerAdmin(admin.ModelAdmin):
  model = UserAnswerExplainer
  list_display = ['user', 'poll', 'question', 'status', 'count_touched']
  actions = None

  def has_add_permission(self, request):
    return False

  def has_change_permission(self, request, obj=None):
    return False

admin.site.register(UserAnswerOpen, UserAnswerOpenAdmin)
admin.site.register(UserAnswerYesOrNo, UserAnswerYesOrNoAdmin)
admin.site.register(UserAnswerMultiple, UserAnswerMultipleAdmin)
admin.site.register(UserAnswerSingle, UserAnswerSingleAdmin)
admin.site.register(UserAnswerRange, UserAnswerRangeAdmin)
admin.site.register(UserAnswerExplainer, UserAnswerExplainerAdmin)

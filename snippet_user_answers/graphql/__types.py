import graphene
from graphene_django import DjangoObjectType

# Types
from snippet.users.graphql.__types import UserType
from snippet_questions.graphql.__types import QuestionOpenType, QuestionYesOrNoType, QuestionMultipleType, QuestionSingleType, QuestionRangeType, QuestionExplainerType

# Models
from ..models import UserAnswerOpen, UserAnswerYesOrNo, UserAnswerMultiple, UserAnswerSingle, UserAnswerRange, UserAnswerExplainer


class BaseUserAnswerType(graphene.Interface):
  id = graphene.Int()
  status = graphene.Boolean()
  user = graphene.Field(UserType)

class UserAnswerOpenType(DjangoObjectType):
  question = graphene.Field(QuestionOpenType)
  answer_text = graphene.String()

  class Meta:
    model = UserAnswerOpen
    interfaces = [BaseUserAnswerType]
    exclude = ("poll",)


class UserAnswerYesOrNoType(DjangoObjectType):
  question = graphene.Field(QuestionYesOrNoType)
  answer_value = graphene.Int()
  answer_note = graphene.String()

  class Meta:
    model = UserAnswerYesOrNo
    interfaces = [BaseUserAnswerType]
    exclude = ("poll",)


class UserAnswerMultipleType(DjangoObjectType):
  question = graphene.Field(QuestionMultipleType)
  answer_choice_keys = graphene.List(graphene.Int)

  class Meta:
    model = UserAnswerMultiple
    interfaces = [BaseUserAnswerType]
    exclude = ("poll",)


class UserAnswerSingleType(DjangoObjectType):
  question = graphene.Field(QuestionSingleType)
  answer_choice_key = graphene.Int()

  class Meta:
    model = UserAnswerSingle
    interfaces = [BaseUserAnswerType]
    exclude = ("poll",)


class UserAnswerRangeType(DjangoObjectType):
  question = graphene.Field(QuestionRangeType)
  answer_scale_key = graphene.Int()

  class Meta:
    model = UserAnswerRange
    interfaces = [BaseUserAnswerType]
    exclude = ("poll",)


class UserAnswerExplainerType(DjangoObjectType):
  question = graphene.Field(QuestionExplainerType)

  class Meta:
    model = UserAnswerExplainer
    interfaces = [BaseUserAnswerType]
    exclude = ("poll",)


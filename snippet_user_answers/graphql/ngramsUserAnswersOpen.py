import graphene
from graphql_jwt.decorators import login_required
from django.apps import apps as django_apps

import string

import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

# Models
from ..models import UserAnswerOpen


class NgramsUserAnswersOpen(graphene.ObjectType):
  ngrams_user_answers_open = graphene.types.json.JSONString(question_id=graphene.ID(
      required=True), user_ids=graphene.List(graphene.Int), reset_flag=graphene.Boolean())

  @login_required
  def resolve_ngrams_user_answers_open(self, info, question_id, reset_flag, ** kwargs):

    QuestionModel = django_apps.get_model('snippet_questions', 'QuestionOpen')
    question_open = QuestionModel.objects.get(id=question_id)


    user_ids=kwargs.get('user_ids', [])

    # If no (edited) ngrams are saved yet, generate new ngrams
    if question_open.ngrams == None or reset_flag or (len(user_ids) != question_open.filter_user_ids_length):

      # START generate ngrams
      temp_unigrams = []
      temp_bigrams = []


      # Remove stopwords and create ngrams
      stopwords_en = set(stopwords.words("english"))
      stopwords_de = set(stopwords.words("german")).difference(['nicht', 'nichts', 'keine'])
      all_stopwords = stopwords_en.union(stopwords_de)
        
      filter_params = {"question_id":question_id}
      if user_ids: filter_params["user_id__in"] = user_ids

      user_answers_open = UserAnswerOpen.objects.select_related("question").select_related("user").filter(**filter_params)

      for answer in user_answers_open:
        unigrams_no_stopwords = []
        bigrams_no_stopwords = []

        if answer.answer_text:
          blob_no_punct_lower = answer.answer_text.translate(str.maketrans("", "", string.punctuation)).lower()
          words = word_tokenize(blob_no_punct_lower)


          for word in words:
            if word not in all_stopwords:
              unigrams_no_stopwords.append(word)

          bigrams_no_stopwords = nltk.bigrams(unigrams_no_stopwords)

        temp_unigrams += unigrams_no_stopwords
        temp_bigrams += bigrams_no_stopwords


      # Convert tuples from Freqency Distribution to lists for mutability
      def listit(t):
        return list(map(listit, t)) if isinstance(t, (list, tuple)) else t

      unigrams_count = listit(nltk.FreqDist(temp_unigrams).most_common(20))
      bigrams_count = listit(nltk.FreqDist(temp_bigrams).most_common(20))

      # Join the two bigrams strings to one string
      for bigram in bigrams_count:
        bigram[0] = " ".join(bigram[0])

      ngrams_user_answers_open = {'ngrams': unigrams_count + bigrams_count}
      # END generate ngrams
      question_open.ngrams = ngrams_user_answers_open
      question_open.filter_user_ids_length = len(user_ids)
      question_open.save()

    # else return saved ngrams
    else:
      ngrams_user_answers_open = question_open.ngrams

    return ngrams_user_answers_open

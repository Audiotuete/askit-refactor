import graphene
from graphql_jwt.decorators import login_required

import datetime

# Types
from snippet.users.graphql.__types import UserType
from snippet_questions.graphql.__types import QuestionOpenType


# Models
from ..models import UserAnswerOpen


class UpdateUserAnswerOpenMutation(graphene.Mutation):
  question = graphene.Field(QuestionOpenType)
  first_touched = graphene.types.datetime.DateTime()
  last_touched = graphene.types.datetime.DateTime()
  count_touched = graphene.Int()

  status = graphene.Boolean()
  answer_text = graphene.String()
  pass

  class Arguments:
    question_id = graphene.ID(required=True)
    answer_text = graphene.String()

  @login_required
  def mutate(self, info, question_id, answer_text):
    current_user = info.context.user
    pending_answer = UserAnswerOpen.objects.get(
        user=current_user, question_id=question_id, poll_id=current_user.current_poll.id)

    # If answer_text is submitted set/override answer_text and mark question as answered
    if answer_text:
      pending_answer.answer_text = answer_text
      pending_answer.status = True
    # If no answer_text is submitted but there is aleady one mark question as answered
    elif pending_answer.answer_text:
      pending_answer.status = True
    else:
      pending_answer.status = False

    # If answer wasn't answered before set first_touched and last_touched to datetime to now
    if pending_answer.first_touched == None:
      pending_answer.first_touched = datetime.datetime.now()
      pending_answer.last_touched = datetime.datetime.now()
    else:
      pending_answer.last_touched = datetime.datetime.now()

    pending_answer.count_touched += 1
    pending_answer.save()

    return UpdateUserAnswerOpenMutation(
        status=pending_answer.status,
        question=pending_answer.question,
        first_touched=pending_answer.first_touched,
        last_touched=pending_answer.last_touched,
        count_touched=pending_answer.count_touched,
        answer_text=pending_answer.answer_text
    )


class UpdateUserAnswerOpen(graphene.ObjectType):
  update_user_answer_open = UpdateUserAnswerOpenMutation.Field()

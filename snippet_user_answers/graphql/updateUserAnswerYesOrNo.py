import graphene
from graphql_jwt.decorators import login_required

import datetime

# Types
from snippet.users.graphql.__types import UserType
from snippet_questions.graphql.__types import QuestionYesOrNoType


# Models
from ..models import UserAnswerYesOrNo


class UpdateUserAnswerYesOrNoMutation(graphene.Mutation):
  question = graphene.Field(QuestionYesOrNoType)
  first_touched = graphene.types.datetime.DateTime()
  last_touched = graphene.types.datetime.DateTime()
  count_touched = graphene.Int()

  status = graphene.Boolean()
  answer_value = graphene.Int()
  answer_note = graphene.String()

  class Arguments:
    question_id = graphene.ID(required=True)
    answer_value = graphene.Int(required=False)
    answer_note = graphene.String()

  @login_required
  def mutate(self, info, question_id, answer_value, answer_note):
    current_user = info.context.user
    pending_answer = UserAnswerYesOrNo.objects.get(
        user=current_user, question_id=question_id, poll_id=current_user.current_poll.id)

    values_dict = {
        # 'NO': 0,
        # 'YES': 1',
        'NOTE': 9,
    }

    # If there is an answer_note set answer_note and set answer_value to 'Note' (9)
    if answer_note:
      pending_answer.answer_note = answer_note
      pending_answer.answer_value = values_dict['NOTE']
    # If there is no answer_note check if answer value is valid
    elif answer_value < 0 or answer_value > 1:
      raise Exception('Choice not available')
    # If answer_value is valid set answer_value and reset answer_note to None
    else:
      pending_answer.answer_value = answer_value
      pending_answer.answer_note = None

    if pending_answer.answer_value > -1:
      pending_answer.status = True
    else:
      pending_answer.status = False

  # If answer wasn't answered before set first_touched and last_touched to datetime to now
    if pending_answer.first_touched == None:
      pending_answer.first_touched = datetime.datetime.now()
      pending_answer.last_touched = datetime.datetime.now()
    else:
      pending_answer.last_touched = datetime.datetime.now()

    pending_answer.count_touched += 1
    pending_answer.save()

    return UpdateUserAnswerYesOrNoMutation(
        status=pending_answer.status,
        question=pending_answer.question,
        first_touched=pending_answer.first_touched,
        last_touched=pending_answer.last_touched,
        count_touched=pending_answer.count_touched,
        answer_value=pending_answer.answer_value,
        answer_note=pending_answer.answer_note
    )


class UpdateUserAnswerYesOrNo(graphene.ObjectType):
  update_user_answer_yes_or_no = UpdateUserAnswerYesOrNoMutation.Field()

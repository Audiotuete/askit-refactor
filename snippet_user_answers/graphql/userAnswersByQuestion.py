import graphene
from graphql_jwt.decorators import login_required
from django.apps import apps as django_apps


# Types
from .__types import BaseUserAnswerType

# Models

class UserAnswersByQuestion(graphene.ObjectType):
  user_answers_by_question = graphene.List(BaseUserAnswerType, 
    question_id=graphene.ID(required=True), 
    question_type=graphene.String(required=True),
    user_ids = graphene.List(graphene.Int)
  )

  @login_required
  def resolve_user_answers_by_question(self, info, question_id, question_type, ** kwargs):

    UserAnswerModel = django_apps.get_model('snippet_user_answers', 'UserAnswer' + question_type)
    filter_params = {"question_id":question_id}

    user_ids=kwargs.get('user_ids', None)
    if user_ids != None:
      filter_params["user_id__in"] = user_ids

      user_answers_by_question = UserAnswerModel.objects.select_related("question").select_related("user").filter(**filter_params)


    else:
      user_answers_by_question = UserAnswerModel.objects.select_related("question").filter(**filter_params)

    return user_answers_by_question

from importlib.metadata import requires
import graphene
from graphql_jwt.decorators import login_required
from django.apps import apps as django_apps


from ..models import Poll

class UpdatePollFilterQuestionMutation(graphene.Mutation):
  user_ids = graphene.List(graphene.Int)

  class Arguments:
    poll_id=graphene.ID(required=True)
    question_id=graphene.ID(required=True)
    question_type=graphene.String(required=True)
    selected_values = graphene.List(graphene.Int, required=True)
    reset = graphene.Boolean(required=True)


  @login_required
  def mutate(self, info, poll_id, question_id, question_type, selected_values, reset):

    user_ids = []

    poll = Poll.objects.get(id=poll_id)

    if reset == True:
      poll.poll_filter_question_id = None
      poll.poll_filter_question_type = None
      poll.poll_filter_question_selected_values = None
      poll.poll_filter_question_user_ids = None

    else:
      UserAnswerModel = django_apps.get_model('snippet_user_answers', 'UserAnswer' + question_type)

      filter_params = {"question_id":question_id, "status":True}

      if question_type == "YesOrNo": filter_params["answer_value__in"] = selected_values
      elif question_type == "Single": filter_params["answer_choice_key__in"] = selected_values
      elif question_type == "Multiple": filter_params["answer_choice_keys__overlap"] = selected_values

      user_answers = UserAnswerModel.objects.select_related("user").filter(**filter_params)
      for answer in user_answers:
        user_ids.append(answer.user.id)

      poll.poll_filter_question_id = question_id
      poll.poll_filter_question_type = question_type
      poll.poll_filter_question_selected_values = selected_values
      poll.poll_filter_question_user_ids = user_ids

    poll.save()

    return UpdatePollFilterQuestionMutation(user_ids=user_ids)

class UpdatePollFilterQuestion(graphene.ObjectType):
  update_poll_filter_question = UpdatePollFilterQuestionMutation.Field()
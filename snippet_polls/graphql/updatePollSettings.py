import graphene
from graphql_jwt.decorators import login_required


# Types
from .__types import PollType

# Models
from ..models import Poll


class UpdatePollSettingsMutation(graphene.Mutation):
  poll = graphene.Field(PollType)

  class Arguments:
    poll_id = graphene.ID(required=True)
    poll_status = graphene.String(required=True)
    poll_name = graphene.String(required=True)
    poll_description = graphene.String()
    poll_language = graphene.String(required=True)
    poll_color_primary = graphene.String(required=True)
    poll_color_secundary = graphene.String(required=True)
    poll_startscreen = graphene.String(required=True)
    poll_impress = graphene.String(required=True)
    poll_support = graphene.String(required=True)
    poll_privacy = graphene.String(required=True)
    poll_missing_modal = graphene.String(required=True)
    poll_success_modal = graphene.String(required=True)
    poll_success_button = graphene.String(required=True)
    poll_redirect_url = graphene.String(required=True)
    poll_redirect_btn_text = graphene.String(required=True)
    poll_email_usage = graphene.String(required=True)
    poll_email_header = graphene.String(required=True)
    poll_participation_terms = graphene.String(required=True)

  @login_required
  def mutate(self, info,
             poll_id,
             poll_status,
             poll_name,
             poll_description,
             poll_language,
             poll_color_primary,
             poll_color_secundary,
             poll_startscreen,
             poll_impress,
             poll_support,
             poll_privacy,
             poll_missing_modal,
             poll_success_modal,
             poll_success_button,
             poll_redirect_url,
             poll_redirect_btn_text,
             poll_email_usage,
             poll_email_header,
             poll_participation_terms,
             **kwargs):

    poll = Poll.objects.get(id=poll_id)

    poll.poll_status = Poll.Status[poll_status.upper()]
    poll.poll_name = poll_name
    poll.poll_description = poll_description
    poll.poll_language = Poll.Language[poll_language.upper()]
    poll.poll_color_primary = poll_color_primary
    poll.poll_color_secundary = poll_color_secundary
    poll.poll_startscreen = poll_startscreen
    poll.poll_impress = poll_impress
    poll.poll_support = poll_support
    poll.poll_privacy = poll_privacy
    poll.poll_missing_modal = poll_missing_modal
    poll.poll_success_modal = poll_success_modal
    poll.poll_success_button = Poll.SuccessButton[poll_success_button.upper()]
    poll.poll_redirect_url = poll_redirect_url
    poll.poll_redirect_btn_text = poll_redirect_btn_text
    poll.poll_email_usage = poll_email_usage
    poll.poll_email_header = poll_email_header
    poll.poll_participation_terms = poll_participation_terms
    poll.save()

    # CRAZY bulk update hack for img-urls after media url change
    # all_polls = Poll.objects.all()
    # for item in all_polls:
    #   item.poll_startscreen = item.poll_startscreen.replace("data.snippet-live.de", "challenge-app-space.fra1.cdn.digitaloceanspaces.com")
    #   item.poll_impress = item.poll_impress.replace("data.snippet-live.de", "challenge-app-space.fra1.cdn.digitaloceanspaces.com")
    #   item.save()

    return UpdatePollSettingsMutation(poll=poll)


class UpdatePollSettings(graphene.ObjectType):
  update_poll_settings = UpdatePollSettingsMutation.Field()

from django.contrib.auth import get_user_model

import graphene
from graphql_jwt.decorators import login_required

# Models
from ..models import Poll


class ResetPollMutation(graphene.Mutation):
  success = graphene.Boolean()

  class Arguments:
    poll_id = graphene.ID(required=True)

  @login_required
  def mutate(self, info, poll_id):

    User = get_user_model()
    poll = Poll.objects.get(id=poll_id)
    poll.poll_participants_counter = 0
    poll.poll_status = Poll.Status.INACTIVE
    poll.save()
    
    user_deletes = User.objects.filter(current_poll_id=poll_id).exclude(is_superuser=True).exclude(groups__name="Manager")
    if poll.poll_owner:
      user_deletes.exclude(id=poll.poll_owner.id)

    user_deletes.delete()    

    success = True
    return ResetPollMutation(success=success)


class ResetPoll(graphene.ObjectType):
  reset_poll = ResetPollMutation.Field()

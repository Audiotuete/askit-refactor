from django.contrib.auth import get_user_model

import graphene
from graphql_jwt.decorators import login_required

# Models
from ..models import Poll


class ChangePollStatusMutation(graphene.Mutation):
  poll_status = graphene.String()

  class Arguments:
    poll_id = graphene.ID(required=True)
    poll_status = graphene.String(required=True)
    reset_poll_flag = graphene.Boolean(required=True, default_value=False)

  @login_required
  def mutate(self, info, poll_id, poll_status, reset_poll_flag):

    poll = Poll.objects.get(id=poll_id)
    poll.poll_status = Poll.Status[poll_status]
    poll.save()

    if(reset_poll_flag):
      User = get_user_model()
      User.objects.filter(current_poll_id=poll_id).exclude(is_superuser=True).exclude(id=poll.poll_owner.id).exclude(groups__name="Manager").delete()

    return ChangePollStatusMutation(poll_status=poll_status)


class ChangePollStatus(graphene.ObjectType):
  change_poll_status = ChangePollStatusMutation.Field()

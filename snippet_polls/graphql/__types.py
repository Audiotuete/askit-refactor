from graphene_django import DjangoObjectType

# Models
from ..models import Poll


class PollType(DjangoObjectType):
  class Meta:
    model = Poll
    fields = ('id',
              'poll_status',
              'poll_is_multipoll',
              'poll_requires_registration',
              'poll_requires_tan',
              'poll_name',
              'poll_description',
              'poll_color_primary',
              'poll_color_secundary',
              'ordered_question_ids',
              'poll_language',
              'poll_impress',
              'poll_support',
              'poll_privacy',
              'poll_startscreen',
              'poll_missing_modal',
              'poll_success_modal',
              'poll_success_button',
              'poll_redirect_url',
              'poll_redirect_btn_text',
              'poll_email_usage',
              'poll_email_header',
              'poll_participation_terms',
              'poll_code',
              'poll_set',
              'poll_participants_counter',
              'poll_success_emails_counter',
              'poll_filter_question_id',
              'poll_filter_question_type',
              'poll_filter_question_selected_values',
              'poll_filter_question_user_ids',
              )

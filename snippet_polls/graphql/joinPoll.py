import graphene
from graphql_jwt.decorators import login_required


# Types
from .__types import PollType

# Models
from ..models import Poll


class JoinPollMutation(graphene.Mutation):
  poll = graphene.Field(PollType)

  class Arguments:
    pollCode = graphene.String(required=True)

  @login_required
  def mutate(self, info, pollCode):

    poll_to_join = Poll.objects.get(poll_code=pollCode)

    # Connect user to poll
    user = info.context.user
    user.current_poll = poll_to_join
    user.save()

    return JoinPollMutation(poll=poll_to_join)


class JoinPoll(graphene.ObjectType):
  join_poll = JoinPollMutation.Field()

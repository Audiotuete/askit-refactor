from django.apps import apps as django_apps

import graphene
from graphql_jwt.decorators import permission_required

# Types
from .__types import PollType

# Models
from ..models import Poll
from snippet_user_polls.models import UserPoll


class PollsByUser(graphene.ObjectType):
  polls_by_user = graphene.List(PollType)

  @permission_required('snippet_polls.view_poll')
  def resolve_polls_by_user(self, info, **kwargs):

    if info.context.user.is_superuser:
      polls = Poll.objects.all().order_by('poll_name')
    else:
      polls = Poll.objects.filter(poll_owner=info.context.user).order_by('poll_name')

    # Update participant_counter and success_emails_counter
    for poll in polls:
      success_emails_counter = UserPoll.objects.select_related(
          "poll").filter(poll=poll).exclude(success_email__exact='').count()

      poll.poll_success_emails_counter = success_emails_counter
      poll.save()

    return polls

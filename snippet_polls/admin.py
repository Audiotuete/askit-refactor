from django.contrib import admin

from .models import Poll


class PollAdmin(admin.ModelAdmin):
  model = Poll
  list_display = ('poll_name', 'id', 'poll_code', 'poll_owner', 'poll_status', 'created_at', 'updated_at',)
  exclude = ['poll_is_multipoll', 'poll_requires_registration','poll_language' ,'poll_color_primary' ,'poll_color_secundary' ,'parent_poll' ]



  search_fields = ['poll_name', 'poll_owner__username']


  readonly_fields = [
      'poll_code',
      # 'ordered_question_ids'
  ]
  # actions = None

  # def has_add_permission(self, request):
  #   return False
  # def has_delete_permission(self, request, obj=None):
  #   return False


# Register your models here.
admin.site.register(Poll, PollAdmin)

# Generated by Django 3.0.5 on 2022-11-07 14:14

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_polls', '0040_auto_20221107_1504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='poll',
            name='poll_support',
            field=ckeditor.fields.RichTextField(blank=True, default='hilfe@askit-app.de', verbose_name='Support'),
        ),
    ]

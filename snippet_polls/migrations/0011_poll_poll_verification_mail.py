# Generated by Django 3.0.5 on 2020-08-28 11:18

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_polls', '0010_auto_20200828_0911'),
    ]

    operations = [
        migrations.AddField(
            model_name='poll',
            name='poll_verification_mail',
            field=ckeditor.fields.RichTextField(blank=True, null=True, verbose_name='Bestätigungsmail'),
        ),
    ]

import graphene

# Mail
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import Site

from django.apps import apps as django_apps
from django.contrib.auth import get_user_model
from django.utils.crypto import get_random_string
from django.conf import settings


# Types
from .__types import AccountType

# Models
from ..models import Account
from snippet_notifications.models import NotificationEmail


class CreateAccountMutation(graphene.Mutation):
  account = graphene.Field(AccountType)

  class Arguments:
    username = graphene.String(required=True)
    email = graphene.String(required=True)
    tier_id = graphene.Int(required=True)
    renewal_date = graphene.DateTime(required=True)
    expiration_date = graphene.DateTime(required=True)
    subscription_id = graphene.Int(required=True)

  # TODO @specific_IP_required
  def mutate(self, info, username, email, tier_id, renewal_date, expiration_date, subscription_id, **kwargs):

    if info.context.META.get("HTTP_ORIGIN") not in settings.CORS_ORIGIN_WHITELIST:
      raise Exception(info.context.META.get("HTTP_ORIGIN"))
    
    tier = get_tier_by_id(tier_id)

    # Create new user with placeholder poll
    User = get_user_model()
    try: 
      user = User.objects.get(username=username)
      account = Account.objects.get(user=user)
      
      account.active = True
      account.tier = tier
      account.renewal_date = renewal_date
      account.expiration_date = expiration_date
      account.woo_subscription_id = subscription_id
      account.save()

    except :
      user = User(
          username=username,
          unique_identifier=username,
      )
      password = get_random_string(length=10)
      user.set_password(password)
      user.save()

      account = Account(
          active=True,
          email=email,
          user=user,
          tier=tier,
          renewal_date=renewal_date,
          expiration_date=expiration_date,
          woo_subscription_id = subscription_id
      )
      account.save()

      email_notification = NotificationEmail.objects.get(slug="login-data")

      subject = email_notification.subject
      html_message = render_to_string('emails/email_main.html', {
          'domain': Site.objects.get_current().domain,
          'user_name': username,
          'tier': tier.name,
          'token': default_token_generator.make_token(user),
          'uidb64': urlsafe_base64_encode(force_bytes(user.pk)),
          'subject': email_notification.subject,
          'content': email_notification.content,
      })
      # ------------------------------------------

      plain_message = strip_tags(html_message)
      from_email = 'info@askit-app.de'
      to_email = email
      send_mail(subject, plain_message, from_email, [to_email], html_message=html_message)

    return CreateAccountMutation(account=account)


class CreateAccount(graphene.ObjectType):
  create_account = CreateAccountMutation.Field()

# Create user helper functions


def get_poll_by_code(poll_code):
  Poll = django_apps.get_model('snippet_polls', 'Poll')
  return Poll.objects.get(poll_code=poll_code)


def get_tier_by_id(tier_id):
  Tier = django_apps.get_model('snippet_tiers', 'Tier')
  return Tier.objects.get(id=tier_id)

# if len(username) < 3:
#   raise Exception('Username must have at least 3 characters!')
# if len(password) < 8:
#   raise Exception('The password must be at least 8 characters long!')
# if User.objects.filter(username = username_lowercase):
#   raise Exception('Username already exists!')

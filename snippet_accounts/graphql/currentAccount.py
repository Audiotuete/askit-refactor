import graphene
from graphql_jwt.decorators import login_required

# Types
from .__types import AccountType

# Models
from ..models import Account


class CurrentAccount(graphene.ObjectType):

  current_account = graphene.Field(AccountType)

  @login_required
  def resolve_current_account(self, info, **kwargs):

    current_account = Account.objects.get(user=info.context.user)

    return current_account

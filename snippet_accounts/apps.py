from django.apps import AppConfig


class SnippetAccountsConfig(AppConfig):
    name = 'snippet_accounts'

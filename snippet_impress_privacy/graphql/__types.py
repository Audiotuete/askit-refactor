from graphene_django import DjangoObjectType

# Models
from ..models import Impress, PrivacyPolicy, PrivacyTopic


class ImpressType(DjangoObjectType):
  class Meta:
    model = Impress
    fields = ('id',
              'content', 
              )

class PrivacyPolicyType(DjangoObjectType):
  class Meta:
    model = PrivacyPolicy
    fields = ('id',
              'intro',
              'privacytopic_set'
              )

class PrivacyTopicType(DjangoObjectType):
  class Meta:
    model = PrivacyTopic
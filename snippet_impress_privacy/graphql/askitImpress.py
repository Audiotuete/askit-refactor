from django.apps import apps as django_apps

import graphene
from graphql_jwt.decorators import login_required

from ..models import Impress, PrivacyPolicy

from .__types import ImpressType, PrivacyPolicyType


class CombinedResolvers(graphene.ObjectType):
    askit_impress = graphene.Field(ImpressType)
    askit_privacy_policy = graphene.Field(PrivacyPolicyType)

    @login_required
    def resolve_askit_impress(self, info, **kwargs):
      try:
        askit_impress = Impress.objects.get(account__user=1)
      except:
        askit_impress = None
      return askit_impress 

    @login_required
    def resolve_askit_privacy_policy(self, info, **kwargs):
      try:
        askit_privacy_policy = PrivacyPolicy.objects.get(account__user=1)
      except:
        askit_privacy_policy = None
      return askit_privacy_policy 


class AskitImpress(graphene.ObjectType):
  askit_impress_privacy = graphene.Field(CombinedResolvers)

  @login_required
  def resolve_askit_impress_privacy(self, info, **kwargs):

    return CombinedResolvers()

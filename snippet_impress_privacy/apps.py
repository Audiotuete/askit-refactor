from django.apps import AppConfig


class SnippetImpressPrivacyConfig(AppConfig):
    name = 'snippet_impress_privacy'

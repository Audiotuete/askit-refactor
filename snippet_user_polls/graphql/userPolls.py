import graphene
from graphql_jwt.decorators import login_required

# Types
from .__types import UserPollType

# Models
from ..models import UserPoll


class UserPolls(graphene.ObjectType):

  user_polls = graphene.List(UserPollType, poll_id=graphene.ID(required=True))

  @login_required
  def resolve_user_polls(self, info, poll_id, **kwarg):

    match_user_polls = UserPoll.objects.filter(poll_id=poll_id)
    return match_user_polls

import graphene
from graphene_django import DjangoObjectType

# Models
from ..models import UserPoll


class UserPollType(DjangoObjectType):
  class Meta:
    model = UserPoll
    fields = ('id',
              'success_email',
              )


# class AdminPollType(DjangoObjectType):
#   class Meta:
#     model = Poll
#     fields = ('id',
#               'poll_name',
#               'poll_color_primary',
#               'poll_color_secundary',
#               'ordered_question_ids',
#               'poll_language',
#               'poll_impress',
#               'poll_support',
#               'poll_privacy',
#               'poll_startscreen',
#               'poll_missing_modal',
#               'poll_success_modal',
#               'poll_success_button',
#               'poll_success_button_url',
#               )

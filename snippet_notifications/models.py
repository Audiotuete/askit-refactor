from django.db import models
from ckeditor.fields import RichTextField


class NotificationEmail(models.Model):

  class Meta:
    verbose_name = 'E-Mail'
    verbose_name_plural = 'E-Mails'

  subject = models.CharField(("E-Mail-Betreff"), max_length=250)
  content = RichTextField(("E-Mail-Inhalt"), default="", config_name='extended')
  slug = models.CharField(("slug"), max_length=100)
  description = models.TextField(("Interne Anmerkungen"), blank=True, max_length=500)


  def __str__(self):
    return self.subject

class NotificationHint(models.Model):

  class Meta:
    verbose_name = 'Hinweis'
    verbose_name_plural = 'Hinweise'

  slug = models.CharField(("slug"), max_length=100)
  text = RichTextField(("Hinweis"), default="", blank=True)

  def __str__(self):
    return self.slug
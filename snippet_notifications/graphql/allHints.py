import graphene

# Types
from .__types import NotificationHintType

# Models
from ..models import NotificationHint

class AllHints(graphene.ObjectType):
  all_hints = graphene.List(NotificationHintType)

  def resolve_all_hints(self, info, **kwargs):
    return NotificationHint.objects.all()

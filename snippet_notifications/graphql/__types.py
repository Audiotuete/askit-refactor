from graphene_django import DjangoObjectType

# Models
from ..models import NotificationHint


class NotificationHintType(DjangoObjectType):
  class Meta:
    model = NotificationHint
    fields = ('slug','text')

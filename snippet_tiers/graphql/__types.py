from graphene_django import DjangoObjectType

# Models
from ..models import Tier, TierQuestionType, TierEndAction


class TierType(DjangoObjectType):
  class Meta:
    model = Tier
    fields = ('id',
              'name',
              'polls_amount',
              'questions_amount',
              'participants_amount',
              'user_answers_amount',
              'image_uploads_amount',
              'conditional_analysis',
              'wordcloud_editor',
              'disable_upgrade',
              'email_support',
              'end_actions',
              'question_types',
              )


class TierQuestionTypeType(DjangoObjectType):
  class Meta:
    model = TierQuestionType


class TierEndActionType(DjangoObjectType):
  class Meta:
    model = TierEndAction

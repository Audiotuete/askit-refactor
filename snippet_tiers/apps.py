from django.apps import AppConfig

class SnippetTiersConfig(AppConfig):
    name = 'snippet_tiers'

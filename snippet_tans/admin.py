from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from snippet_tans.models import Tanlist, Tan


class TanListAdmin(admin.ModelAdmin):
  model = Tanlist
  list_display = ('id', 'poll')

  # def has_delete_permission(self, request, obj=None):
    # return False

class TanAdmin(ImportExportModelAdmin):
  model = Tan
  list_display = ('tan', 'poll', 'used', 'valid_vote')
  # readonly_fields = ['tan', 'poll',]
  search_fields = ['tan', 'poll__poll_name',]

  # def has_add_permission(self, request):
  #   return False
  # def has_delete_permission(self, request, obj=None):
    # return False

admin.site.register(Tanlist, TanListAdmin)
admin.site.register(Tan, TanAdmin)


# Generated by Django 3.0.5 on 2022-04-22 11:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_tans', '0006_auto_20220421_1922'),
    ]

    operations = [
        migrations.AddField(
            model_name='tan',
            name='valid_vote',
            field=models.BooleanField(default=True),
        ),
    ]

# Generated by Django 3.0.5 on 2022-03-21 16:49

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('snippet_polls', '0032_remove_poll_poll_is_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tanlist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('poll', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='snippet_polls.Poll')),
            ],
        ),
        migrations.CreateModel(
            name='Tan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tan', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(999999)])),
                ('status', models.BooleanField(default=True)),
                ('tanlist', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='snippet_tans.Tanlist', verbose_name='TAN-List')),
            ],
            options={
                'unique_together': {('tanlist', 'tan')},
            },
        ),
    ]

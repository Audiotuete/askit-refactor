from django.db import models
from django.core.validators import MaxValueValidator

from random import randint



class Tan(models.Model):
  tanlist = models.ForeignKey('snippet_tans.Tanlist', on_delete=models.CASCADE, verbose_name="TAN-List")
  poll = models.ForeignKey('snippet_polls.Poll', on_delete=models.PROTECT, null=True)
  tan = models.IntegerField(validators=[MaxValueValidator(999999)])
  used = models.BooleanField(default=False)
  valid_vote = models.BooleanField(default=False)

  class Meta:
    unique_together = ['poll', 'tan']
  
  def __str__(self):
    return str(self.tan)

class Tanlist(models.Model):
  poll = models.OneToOneField('snippet_polls.Poll', on_delete=models.PROTECT, null=True, blank=True)
  tan_amount = models.IntegerField(validators=[MaxValueValidator(50000)], default=0)
  # tan_set = ArrayField(models.IntegerField(blank=True), default=list, null=True, blank=True)

  def __str__(self):
    return self.poll.poll_name

  def save(self, *args, **kwargs):
    super(Tanlist, self).save(*args, **kwargs)

    old_amount = Tan.objects.filter(tanlist=self).count()
    new_amount = self.tan_amount

    added_amount = new_amount - old_amount

    tan_set = set()
    while len(tan_set) < added_amount:
      tan_set.add(randint(100000, 999999))

    if added_amount > 0:
      for tan in tan_set:
        tan = Tan(
          tanlist = self,
          poll = self.poll,
          tan = tan
        )
        tan.save()
          









